/**
 * Created by rranjan on 11/29/15.
 */
(function () {
  var zmq = require('zmq');
  var pulseDataAdapter = require('../app/pulseDataAdapter');
  /*
   portPOI:'4002',
   portLoitering:'4003',
   portAuto:'4004',
   portCrowd:4005
   */
  var zmqPushPull= function (config, io) {

    //var zmqsockPOI = zmq.socket('pull');

    //var zmqPortPart = 'tcp://' + config.zmq.host + ':' //+config.zmq.port

    startPulling("portPOI", "wanted");
    startPulling("portLoitering", "loitering");
    startPulling("portAuto", "auto");
    startPulling("portCrowd", "mobCrowd");
    startPullingPulseData("portAlert","alertRecHost");
    startPullingPulseData("portEvent","eventRecHost");



    function startPulling(portName, emitPort){
      var zmqSocket = zmq.socket('pull');
      var zmqPortPart = 'tcp://' + config.zmq.recHost + ':' //+config.zmq.port

      zmqSocket.bindSync(zmqPortPart + config.zmq[portName], function (err) {
        if (err)console.log(err.stack)
      });
      console.log("adding a listner "+zmqPortPart + config.zmq[portName])

      zmqSocket.on("message", function(message) {
        console.log("new message "+ portName)
        console.log(message)
        io.emit(emitPort, JSON.parse(message.toString()))
      })
    }
    function startPullingPulseData(portName, emitPort){
      var zmq = require('zmq'),
      zmqPortPart = 'tcp://'+config.zmq[emitPort]+':'+config.zmq[portName],
      sock = zmq.socket('sub');
      sock.connect(zmqPortPart)
      sock.subscribe("");
      console.log("adding a Listener  "+zmqPortPart)
      sock.on("message", function(message) {
        try{
          pulseDataAdapter[emitPort].buildObject(message)
        }
        catch(err){
          console.log("Error")
          console.log(err.stack)
        }
      })
    }
  }

  module.exports = {zmqPushPull:zmqPushPull}
})()
