/**
 * Created by rranjan on 11/29/15.
 */
(function(){
  var zmq = require('zmq');
  /*
   portPOI:'4002',
   portLoitering:'4003',
   portAuto:'4004',
   portCrouwed:4005
   */
  module.exports = function(config,io){
    var zmqsock = zmq.socket('sub');

    var zmqport = 'tcp://'+config.zmq.host+':'+config.zmq.port

    console.log("zmqport="+zmqport)
    zmqsock.connect(zmqport,function(err){if(err)console.log(err.stack)});

    zmqsock.subscribe('auto');

    console.log("sub to msg");

    zmqsock.on("message",function(topic,message ){
      console.log("new message")
      //console.log(message);
      //console.log(message.toString());
      io.emit('auto',JSON.parse(message.toString()))
    })



    var zmqsockwanted = zmq.socket('sub');
    zmqsockwanted.connect(zmqport,
      function(err){if(err)console.log(err.stack)});

    zmqsockwanted.subscribe('wanted');

    zmqsockwanted.on("message",function(topic,message ){
      console.log("new wanted message")
      //console.log(message);
      //console.log(message.toString());
      io.emit('wanted',JSON.parse(message.toString()))
    })


  }
})()


