var path = require('path'),
  rootPath = path.normalize(__dirname + '/..'),
  env = process.env.NODE_ENV || 'development';

var config = {
  development: {
    root: rootPath,
    app: {
      name: 'suratpolice'
    },
    port: 5000,
  db: 'mongodb://localhost/suratpolice-development',/*
  db: 'mongodb://163.172.131.83:28018/suratpolice',*/
    zmq:{
      //sendHost:"127.0.0.1",
      //recHost:"127.0.0.1",
      sendHost:"127.0.0.1",
      alertRecHost:"13.92.112.165",
      eventRecHost:"13.92.112.165",
      recHost:"*",
      //host:'127.0.0.1',
      port:'4001',
      portPOI:'4002',
      portLoitering:'4003',
      portAuto:'4004',
      portCrowd:'4005',
      portAlert:'8080',
      portEvent:'8081'
    }
  },
  integration: {
    root: rootPath,
    app: {
      name: 'suratpolice'
    },
    port: 4000,
    db: 'mongodb://163.172.131.83:28018/suratpolice',/*
    db: 'mongodb://suratpolice:suratpolicePassword@ds027335.mongolab.com:27335/suratpolice',*/
    zmq:{
      //sendHost:"127.0.0.1",
      //recHost:"127.0.0.1",
      alertRecHost:"13.92.112.165",
      eventRecHost:"13.92.112.165",
      sendHost:"212.47.249.75",
      recHost:"*",
      //host:'127.0.0.1',
      port:'4001',
      portPOI:'4002',
      portLoitering:'4003',
      portAuto:'4004',
      portCrowd:'4005',
      portAlert:'8080',
      portEvent:'8081'
    }
  },
  test: {
    root: rootPath,
    app: {
      name: 'suratpolice'
    },
    port: 4000,
    db: 'mongodb://localhost/suratpolice-test',
    zmq:{
      host:'localhost',
      port:'4001'

    }
  },

  production: {
    root: rootPath,
    app: {
      name: 'suratpolice'
    },
    port: 4000,
    db: 'mongodb://localhost/suratpolice-production',
    zmq:{
      host:'localhost',
      port:'4001'

    }
  }

};

module.exports = config[env];
