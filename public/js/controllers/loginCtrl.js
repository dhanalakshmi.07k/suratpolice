/**
 * Created by Suhas on 12/5/2015.
 */
/*suratCamControllers.controller('loginCtrl', function ($scope,$state) {
  console.log('iiiiiiiiiii');
 $scope.checkValidation = function(){
   console.log('iiiiiiiiiii');
   if($scope.username==='admin' && $scope.password==='gujaratpolice'){
     $state.go('map.live')
   }
 }
})*/

suratCamControllers.controller('loginCtrl', function ($scope,$http,$window,$location,$state) {
  $scope.checkValidation = function (user) {
    console.log("_____inside login_____________");
    console.log(user);
    $http
      .post('/login', user)
      .success(function (data, status, headers, config) {
        console.log(data.token);
        $window.sessionStorage.token = data.token;
        $state.go('map.live')

      })
      .error(function (data, status, headers, config) {
        // Erase the token if the user fails to log in
        delete $window.sessionStorage.token;
        $scope.isAuthenticated = false;
        // Handle login errors here
        $scope.error = 'Error: Invalid user or password';
        $scope.welcome = '';
      });
  };

  $scope.logout = function () {
    delete $window.sessionStorage.token;
    console.log( $window.sessionStorage.token);
    $state.go('/login')
  };
  $scope.signUp = function (user) {
    console.log("_____inside login_____________");
    console.log(user);
    $http
      .post('/signup', user)
      .success(function (data, status, headers, config) {
        console.log(data);
        $scope.successMessage="Successfully registered Please login";
        $state.go('/login')
      })
      .error(function (data, status, headers, config) {
        $scope.error =data ;
      });
  };
});

