/**
 * Created by Suhas on 11/26/2015.
 */

suratCamControllers.controller('settingsController', function ($scope,settingService,liveDataService) {
  //incial values
  $scope.isNotificationSimulation = settingService.getNotificationSimulation();
  $scope.isNotificationLoteringSimulation = settingService.getLoteringNotificationSimulation();
  $scope.isWantedNotificationSimulation = settingService.getWantedNotificationSimulation();
  $scope.isAutoNotificationSimulation = settingService.getAutoNotificationSimulation();
  $scope.isMobNotificationSimulation = settingService.getMobNotificationSimulation();
  $scope.isLoteringSimulation = settingService.getMobNotificationSimulation();

  $scope.simulateNotification = function(){
    var flag = settingService.getNotificationSimulation();
    settingService.setNotificationSimulation(!flag)
    $scope.isNotificationSimulation = !flag;
    if(!flag){
      /*settingService.startNotificationSimulation(flag);*/
      settingService.cancelAllSimulation();
      settingService.startLoiteringSimulation(flag);
      settingService.startPersonOfInterestSimulation(flag);
      settingService.startAutoSimulation(flag);
      settingService.startCrowdSimulation(flag);
      settingService.setAllSimulatorFlag(true);
      $scope.isNotificationSimulation = settingService.getNotificationSimulation();
      $scope.isNotificationLoteringSimulation = settingService.getLoteringNotificationSimulation();
      $scope.isWantedNotificationSimulation = settingService.getWantedNotificationSimulation();
      $scope.isAutoNotificationSimulation = settingService.getAutoNotificationSimulation();
      $scope.isMobNotificationSimulation = settingService.getMobNotificationSimulation();
      $scope.isLoteringSimulation = settingService.getMobNotificationSimulation();
    }
    else{
      /*settingService.cancelNotificationSimulation(flag);*/
      settingService.setAllSimulatorFlag(false);
      settingService.cancelAllSimulation();
      $scope.isNotificationSimulation = settingService.getNotificationSimulation();
      $scope.isNotificationLoteringSimulation = settingService.getLoteringNotificationSimulation();
      $scope.isWantedNotificationSimulation = settingService.getWantedNotificationSimulation();
      $scope.isAutoNotificationSimulation = settingService.getAutoNotificationSimulation();
      $scope.isMobNotificationSimulation = settingService.getMobNotificationSimulation();
      $scope.isLoteringSimulation = settingService.getMobNotificationSimulation();
    }
  }
  $scope.simulateLoteringNotification = function(){
    var flag = settingService.getLoteringNotificationSimulation();
    settingService.setLoteringNotificationSimulation(!flag)
    $scope.isLoteringNotificationSimulation = !flag;
    if(!flag){
      settingService.startLoiteringSimulation(flag);
    }else{
      settingService.cancelLoiteringSimulation(flag);
    }
  }
  $scope.simulateWantedNotification = function(){
    var flag = settingService.getWantedNotificationSimulation();
    settingService.setWantedNotificationSimulation(!flag)
    $scope.isWantedNotificationSimulation = !flag;
    if(!flag){
      settingService.startPersonOfInterestSimulation(flag);
    }else{
      settingService.cancelPersonOfInterestSimulation(flag);
    }
  }
  $scope.simulateAutoNotification = function(){
    var flag = settingService.getAutoNotificationSimulation();
    settingService.setAutoNotificationSimulation(!flag)
    $scope.isAutoNotificationSimulation = !flag;

    if(!flag){
      settingService.startAutoSimulation(flag);
    }else{
      settingService.cancelAutoSimulation(flag);
    }
  }
  $scope.simulateMobNotification = function(){
    var flag = settingService.getMobNotificationSimulation();
    settingService.setMobNotificationSimulation(!flag)
    $scope.isMobNotificationSimulation = !flag;

    if(!flag){
      settingService.startCrowdSimulation(flag);
    }else{
      settingService.cancelCrowdSimulation(flag);
    }
  }
  $scope.clearSimulatorData = function(){
    liveDataService.clearAllData();
    var flag =true;
    $scope.isNotificationSimulation = settingService.getNotificationSimulation();
    $scope.isNotificationLoteringSimulation = settingService.getLoteringNotificationSimulation();
    $scope.isWantedNotificationSimulation = settingService.getWantedNotificationSimulation();
    $scope.isAutoNotificationSimulation = settingService.getAutoNotificationSimulation();
    $scope.isMobNotificationSimulation = settingService.getMobNotificationSimulation();
    $scope.isLoteringSimulation = settingService.getMobNotificationSimulation();
    settingService.setAllSimulatorFlag(false);
    settingService.cancelAllSimulation();

  }
});
