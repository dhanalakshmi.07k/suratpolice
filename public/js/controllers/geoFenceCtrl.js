/**
 * Created by Suhas on 11/25/2015.
 */
suratCamControllers.controller('geoFence', function ($scope) {
    $scope.sal="test";

    var suratMapInit=function() {
        var mapLatLng = {lat: 21.172642, lng: 72.832447};
        var mapOptions = {
            center: mapLatLng,
            scrollwheel: false,
            zoom: 12
        };
        var addressMap = new google.maps.Map(document.getElementById('geoFenceMap'), mapOptions);
    };
    suratMapInit();
});
