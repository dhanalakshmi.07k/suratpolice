/**
 * Created by Suhas on 12/12/2015.
 */
suratCamControllers.controller('mainCtrl', function ($scope,$location,liveDataService) {
  /*var map;*/
  var dataType='';
  $scope.filterType='loitering';
  $scope.notifyMessage = liveDataService.getAllAlerts();
  liveDataService.setLiveMapFilter(dataType)
  $scope.live = {
    count: {
      all: liveDataService.getAllAlertArrayLength() + 4,
      loitering: liveDataService.getLoiteringAlertArrayLength(),
      wanted: liveDataService.getPersonOfInterestAlertArrayLength(),
      auto: liveDataService.getAutoCrowdAlertArrayLength(),
      peoples: liveDataService.getCrowdAlertArrayLength(),
      street_camera: 0,
      body_camera: 4
    }
  }
  $scope.currentNav= function (path) {
    var loc=$location.path();
    $scope.flowType="fullScreen";

    if(loc.includes('content')){
      $scope.flowType="freeFlowMain"
    }

    return loc.includes(path)
  };
  socket.on('auto', function (dataMap) {
    for (var j = 0; j < dataMap.data.autorickshawCount.length; j++) {
      var notifyData = {
        type: "auto",
        cameraLongitude: dataMap.data.cameraLongitude,
        cameraLatitude: dataMap.data.cameraLatitude,
        alertData: dataMap.data.autorickshawCount[j],
        time: dataMap.data["Time Stamp"],
        cameraName: dataMap.data.cameraName,
        typeName: "",
        message: "Around" + dataMap.data.autorickshawCount[j].estimate + "autos in",
        address: dataMap.data.autorickshawCount[j].regionName,
        img: "images/riksha_Notif_smallresized (1).png"
      }
      liveDataService.setAllAlerts(notifyData);
      liveDataService.setAutoCrowdAlerts(notifyData);
      $scope.filterType = liveDataService.getLiveMapFilter();
      $scope.addmarker(notifyData);
      $scope.notifyMessage = liveDataService.getAllAlerts();
      $scope.live.count.all = liveDataService.getAllAlertArrayLength() + 4;
      $scope.live.count.auto = liveDataService.getAutoCrowdAlertArrayLength();
      //$scope.alertType("auto");
      $scope.$apply();
    }
  });
  socket.on('mobCrowd', function (dataMap) {
    for (var j = 0; j < dataMap.data.crowdCount.length; j++) {
      var notifyData = {
        type: "peoples",
        cameraLongitude: dataMap.data.cameraLongitude,
        cameraLatitude: dataMap.data.cameraLatitude,
        alertData: dataMap.data.crowdCount[j],
        time: dataMap.data["Time Stamp"],
        cameraName: dataMap.data.cameraName,
        typeName: "",
        message: "Around" + dataMap.data.crowdCount[j].estimate + "people in",
        address: dataMap.data.crowdCount[j].regionName,
        img: "images/pepole_smallresized (2).png"
      }
      liveDataService.setAllAlerts(notifyData);
      liveDataService.setCrowdAlerts(notifyData);
      $scope.filterType = liveDataService.getLiveMapFilter();
      $scope.addmarker(notifyData);
      $scope.notifyMessage = liveDataService.getAllAlerts();
      $scope.live.count.all = liveDataService.getAllAlertArrayLength() + 4;
      $scope.live.count.peoples = liveDataService.getCrowdAlertArrayLength();
      /* $scope.alertType("peoples");*/
      $scope.$apply();
    }
  });
  socket.on('loitering', function (dataMap) {
    var details = dataMap.data["Loiterer"];
    var notifyData = {
      type: "loitering",
      alertData: dataMap.data.Loiterer,
      typeName: "Loitering",
      message: "Found a person loitering",
      address: details["Camera Name"],
      time: details["Time Stamp"],
      img: "data:image/png;base64," + details.Photo
    };
    liveDataService.setAllAlerts(notifyData);
    liveDataService.setLoiteringAlerts(notifyData);
    $scope.filterType = liveDataService.getLiveMapFilter();
    $scope.notifyMessage = liveDataService.getAllAlerts();
    $scope.addmarker(notifyData);
    /*$scope.alertType("loitering");*/
    $scope.live.count.all = liveDataService.getAllAlertArrayLength() + 4;
    $scope.live.count.loitering = liveDataService.getLoiteringAlertArrayLength();
    $scope.$apply();
  });
  socket.on('wanted', function (dataMap) {
    var details = dataMap.data["Person Of Interest"];
    var notifyData = {
      type: "wanted",
      typeName: "Person of interest",
      message: details['Person ID'],
      address: details["Camera Name"],
      time: details["Time Stamp"],
      alertData: dataMap.data["Person Of Interest"],
      img: "data:image/png;base64," + details.Photo
    }
    liveDataService.setAllAlerts(notifyData);
    liveDataService.setPersonOfInterestAlerts(notifyData);
    $scope.filterType = liveDataService.getLiveMapFilter();
    $scope.notifyMessage = liveDataService.getAllAlerts();
    $scope.addmarker(notifyData);
    /* $scope.alertType("wanted");*/
    $scope.live.count.all = liveDataService.getAllAlertArrayLength() + 4;
    $scope.live.count.wanted = liveDataService.getPersonOfInterestAlertArrayLength();
    $scope.$apply();
  });
  $scope.addmarker = function (data) {
    /*$scope.toggleGroup($scope.dataType);*/
    var img;
    var num = 0, lat, log;
    if (data.type == "peoples" || data.type == "auto") {
      img = "images/" + data.type + ".png";
      num = data.alertData.estimate;
      star = "*";
      lat = data.cameraLatitude;
      log = data.cameraLongitude;
    }
    else {
      img = "data:image/png;base64," + data.alertData['Photo'];
      num = "";
      star = " ";
      lat = data.alertData.Latitude;
      log = data.alertData.Longitude;
    }
    var myOptions = {
      content: '<div class="markerMain  ' + data.type + '">' +
      '<img src="' + img + '" class="markerImg">' +
      '             <div class="markerContMain">' +
      '                 <div class="markerCont">' +
      '<div class="markerText">' +
      '                     <div class="num">' + num + '' + star + '</div>' +
      '                     <div class="txt">' + data.type + '</div>' +
      '</div>' +
      '                  </div>' +
      '             </div>' +
      '        </div>',
      pixelOffset: new google.maps.Size(-28, -30), // left upper corner of the label
      position: new google.maps.LatLng(lat, log),
      closeBoxURL: "",
      type: data.type
    };
    var ib = new InfoBox(myOptions);
    if(data.alertData.Latitude!="0"&&data.alertData.Longitude!="0"){
      var markerData = {
        markers: ib,
        markerType: data.type
      }
      liveDataService.setAllMarker(markerData);
      if (liveDataService.getLiveMapFilter() === '' || liveDataService.getLiveMapFilter() === data.type) {
        ib.open(liveDataService.getLiveMapData());
      }
    }
  };
});
