/**
 * Created by Suhas on 11/26/2015.
 */

    suratCamControllers.controller('notificationCtrl', function ($scope, liveDataService, settingService,$state,authInterceptor) {
        /*$scope.messages = {
            all: [],
            loitering: [],
            wanted: [],
            auto_crowd: [],
            mob_crowd: [],
            street_camera: [],
            body_camera: []
        }
        socket.on('loitering', function (data) {
            var notifyData = {
                type: "Loitering",
                alertData: data.data.Loiterer
            }
            liveDataService.setAllAlerts(notifyData);
            liveDataService.setLoiteringAlerts(notifyData);
            $scope.live.count.all = liveDataService.getAllAlertArrayLength();
            $scope.live.count.loitering = liveDataService.getLoiteringAlertArrayLength();
            if ($scope.is_all_tab) {
                $scope.notifyMessage = liveDataService.getAllAlerts();
            }
            else if ($scope.is_loitering_tab) {
                $scope.notifyMessage = liveDataService.getLoiteringAlerts();
            }
            $scope.$apply();
        });
        socket.on('wanted', function (data) {
            var notifyData = {
                type: "person Of Interest",
                alertData: data.data["Person Of Interest"]
            }
            liveDataService.setAllAlerts(notifyData);
            liveDataService.setPersonOfInterestAlerts(notifyData);
            $scope.live.count.all = liveDataService.getAllAlertArrayLength();
            $scope.live.count.wanted = liveDataService.getPersonOfInterestAlertArrayLength();
            if ($scope.is_all_tab) {
                $scope.notifyMessage = liveDataService.getAllAlerts();
            }
            else if ($scope.is_wanted_tab) {
                $scope.notifyMessage = liveDataService.getPersonOfInterestAlerts();
            }
            $scope.$apply();
        });
        socket.on('auto', function (data) {
            for (var j = 0; j < data.data.autorickshawCount.length; j++) {
                var notifyData = {
                    type: "auto",
                    cameraLongitude: data.data.cameraLongitude,
                    cameraLatitude: data.data.cameraLatitude,
                    alertData: data.data.autorickshawCount[j],
                    time: data.data["Time Stamp"],
                    cameraName: data.data.cameraName

                };
                liveDataService.setAllAlerts(notifyData);
                liveDataService.setAutoCrowdAlerts(notifyData);
            }

            $scope.live.count.all = liveDataService.getAllAlertArrayLength();
            $scope.live.count.auto_crowd = liveDataService.getAutoCrowdAlertArrayLength();
            if ($scope.is_all_tab) {
                $scope.notifyMessage = liveDataService.getAllAlerts();
            }
            else if ($scope.is_auto_crowd_tab) {
                $scope.notifyMessage = liveDataService.getAutoCrowdAlerts();
            }
            $scope.$apply();
        });
        socket.on('mobCrowd', function (data) {
            var mobCrowdArray = [];
            for (var j = 0; j < data.data.crowdCount.length; j++) {
                var notifyData = {
                    type: "mobCrowd",
                    cameraLongitude: data.data.cameraLongitude,
                    cameraLatitude: data.data.cameraLatitude,
                    alertData: data.data.crowdCount[j],
                    time: data.data["Time Stamp"],
                    cameraName: data.data.cameraName
                }
                liveDataService.setAllAlerts(notifyData);
                liveDataService.setCrowdAlerts(notifyData);
            }
            $scope.live.count.all = liveDataService.getAllAlertArrayLength();
            $scope.live.count.mob_crowd = liveDataService.getCrowdAlertArrayLength();
            if ($scope.is_all_tab) {
                $scope.notifyMessage = liveDataService.getAllAlerts();
            }
            else if ($scope.is_mob_detected_tab) {
                $scope.notifyMessage = liveDataService.getCrowdAlerts();
            }
            $scope.$apply();
        });
        $scope.setAllTabFalse = function(){
        $scope.is_loitering_tab = false;
        $scope.is_all_tab = false;
        $scope.is_wanted_tab = false;
        $scope.is_auto_crowd_tab = false;
        $scope.is_mob_detected_tab = false;
        $scope.is_street_camera_tab = false;
        $scope.is_body_camera_tab = false;
      }
        $scope.is_all_tab = true;
        $scope.activetab = function (tabSelected) {
          $scope.setAllTabFalse();
            if (tabSelected == 'is_all_tab') {
                $scope.is_all_tab = true;
                $scope.notifyMessage = liveDataService.getAllAlerts();
            }
            else if (tabSelected == 'is_loitering_tab') {
                $scope.is_loitering_tab = true;
                $scope.notifyMessage = liveDataService.getLoiteringAlerts();
            }
            else if (tabSelected == 'is_wanted_tab') {
                $scope.is_wanted_tab = true;
                $scope.notifyMessage = liveDataService.getPersonOfInterestAlerts();
            }
            else if (tabSelected == 'is_auto_crowd_tab') {
              $scope.is_auto_crowd_tab = true;
              $scope.notifyMessage = liveDataService.getAutoCrowdAlerts();
            }
            else if (tabSelected == 'is_mob_detected_tab') {
              $scope.is_mob_detected_tab = true;
              $scope.notifyMessage = liveDataService.getCrowdAlerts();
            }
            else if (tabSelected == 'is_street_camera_tab') {
                $scope.is_street_camera_tab = true;
            }
            else if (tabSelected == 'is_body_camera_tab') {
                $scope.is_body_camera_tab = true;
            }
        };
        $scope.activetab('is_all_tab');
        $scope.live = {
            count: {
                all: 0,
                loitering: 0,
                wanted: 0,
                auto_crowd: 0,
                mob_crowd: 0,
                street_camera: 4,
                body_camera: 0
            }
        }
        $scope.clearNotificationData = function () {
            liveDataService.clearAllData();
            settingService.cancelAllSimulation();
            $scope.activetab('is_all_tab');
            settingService.setAllSimulatorFlag(false);
            $scope.clearCount();
            $state.go('map.live', {}, { reload: true });
        }
        $scope.clearCount = function () {
            $scope.live = {
                count: {
                    all: 0,
                    loitering: 0,
                    wanted: 0,
                    auto_crowd: 0,
                    mob_crowd: 0,
                    street_camera: 0,
                    body_camera: 0
                }
            }
        }*/
     /* socket.on('wanted', function (data) {
        var notifyData = {
          type: "person Of Interest",
          alertData: data.data["Person Of Interest"]
        }
        liveDataService.setAllAlerts(notifyData);
        liveDataService.setPersonOfInterestAlerts(notifyData);
       /!* $scope.live.count.all = liveDataService.getAllAlertArrayLength();
        $scope.live.count.wanted = liveDataService.getPersonOfInterestAlertArrayLength();*!/
        $scope.notifyMessage = liveDataService.getAllAlerts();
       /!* if ($scope.is_all_tab) {
          $scope.notifyMessage = liveDataService.getAllAlerts();
        }
        else if ($scope.is_wanted_tab) {
          $scope.notifyMessage = liveDataService.getPersonOfInterestAlerts();
        }*!/
        $scope.$apply();
      });*/
    });

