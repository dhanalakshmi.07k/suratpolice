/**
 * Created by Suhas on 11/25/2015.
 */

suratCamControllers.controller('activeNav', function ($scope,$location,liveDataService) {
  var map;
  $scope.dataType='';
  $scope.currentNav= function (path) {
    var loc=$location.path();
    $scope.flowType="fullScreen";

    if(loc.includes('content')){
      $scope.flowType="freeFlowMain"
    }

    return loc.includes(path)
  };
  socket.on('auto', function (dataMap) {
    for (var j = 0; j < dataMap.data.autorickshawCount.length; j++) {
      var notifyData = {
        type: "auto",
        cameraLongitude: dataMap.data.cameraLongitude,
        cameraLatitude: dataMap.data.cameraLatitude,
        alertData: dataMap.data.autorickshawCount[j],
        time: dataMap.data["Time Stamp"],
        cameraName: dataMap.data.cameraName,
        typeName: "",
        message: "Around" + dataMap.data.autorickshawCount[j].estimate + "autos in",
        address: dataMap.data.autorickshawCount[j].regionName,
        img: "images/riksha_Notif_smallresized (1).png"
      }
      liveDataService.setAllAlerts(notifyData);
      liveDataService.setAutoCrowdAlerts(notifyData);
      //$scope.addmarker(notifyData);
      //$scope.notifyMessage = liveDataService.getAllAlerts();
      //$scope.live.count.all = liveDataService.getAllAlertArrayLength() + 4;
      //$scope.live.count.auto = liveDataService.getAutoCrowdAlertArrayLength();
      //$scope.alertType("auto");
      $scope.$apply();
    }
  });
  socket.on('mobCrowd', function (dataMap) {
    for (var j = 0; j < dataMap.data.crowdCount.length; j++) {
      var notifyData = {
        type: "peoples",
        cameraLongitude: dataMap.data.cameraLongitude,
        cameraLatitude: dataMap.data.cameraLatitude,
        alertData: dataMap.data.crowdCount[j],
        time: dataMap.data["Time Stamp"],
        cameraName: dataMap.data.cameraName,
        typeName: "",
        message: "Around" + dataMap.data.crowdCount[j].estimate + "people in",
        address: dataMap.data.crowdCount[j].regionName,
        img: "images/pepole_smallresized (2).png"
      }
      liveDataService.setAllAlerts(notifyData);
      liveDataService.setCrowdAlerts(notifyData);
      //$scope.addmarker(notifyData);
      $scope.notifyMessage = liveDataService.getAllAlerts();
      //$scope.live.count.all = liveDataService.getAllAlertArrayLength() + 4;
      //$scope.live.count.peoples = liveDataService.getCrowdAlertArrayLength();
      /* $scope.alertType("peoples");*/
      $scope.$apply();
    }
  });
  socket.on('loitering', function (dataMap) {
    var details = dataMap.data["Loiterer"];
    var notifyData = {
      type: "loitering",
      alertData: dataMap.data.Loiterer,
      typeName: "Loitering",
      message: "Found a person loitering",
      address: details["Camera Name"],
      time: details["Time Stamp"],
      img: "data:image/png;base64," + details.Photo
    };
    liveDataService.setAllAlerts(notifyData);
    liveDataService.setLoiteringAlerts(notifyData);
    $scope.notifyMessage = liveDataService.getAllAlerts();
    $scope.addmarker(notifyData);
    /*$scope.alertType("loitering");*/
    //$scope.live.count.all = liveDataService.getAllAlertArrayLength() + 4;
    //$scope.live.count.loitering = liveDataService.getLoiteringAlertArrayLength();
    $scope.$apply();

  });
  socket.on('wanted', function (dataMap) {
    var details = dataMap.data["Person Of Interest"];
    var notifyData = {
      type: "wanted",
      typeName: "Person of interest",
      message: details['Person ID'],
      address: details["Camera Name"],
      time: details["Time Stamp"],
      alertData: dataMap.data["Person Of Interest"],
      img: "data:image/png;base64," + details.Photo
    }
    liveDataService.setAllAlerts(notifyData);
    liveDataService.setPersonOfInterestAlerts(notifyData);
    $scope.notifyMessage = liveDataService.getAllAlerts();
    //$scope.addmarker(notifyData);
    /* $scope.alertType("wanted");*/
    //$scope.live.count.all = liveDataService.getAllAlertArrayLength() + 4;
    //$scope.live.count.wanted = liveDataService.getPersonOfInterestAlertArrayLength();
    $scope.$apply();
  });
  $scope.addmarker = function (data) {
    /*$scope.toggleGroup($scope.dataType);*/
    var img;
    var num = 0, lat, log;
    if (data.type == "peoples" || data.type == "auto") {
      img = "images/" + data.type + ".png";
      num = data.alertData.estimate;
      star = "*";
      lat = data.cameraLatitude;
      log = data.cameraLongitude;
    }
    else {
      img = "data:image/png;base64," + data.alertData['Photo'];
      num = "";
      star = " ";
      lat = data.alertData.Latitude;
      log = data.alertData.Longitude;
    }
    var myOptions = {
      content: '<div class="markerMain  ' + data.type + '">' +
      '<img src="' + img + '" class="markerImg">' +
      '             <div class="markerContMain">' +
      '                 <div class="markerCont">' +
      '<div class="markerText">' +
      '                     <div class="num">' + num + '' + star + '</div>' +
      '                     <div class="txt">' + data.type + '</div>' +
      '</div>' +
      '                  </div>' +
      '             </div>' +
      '        </div>',
      pixelOffset: new google.maps.Size(-28, -30), // left upper corner of the label
      position: new google.maps.LatLng(lat, log),
      closeBoxURL: "",
      type: data.type
    };
    var ib = new InfoBox(myOptions);
    var markerData = {
      markers: ib,
      markerType: data.type
    }
    /*if (data.type == "peoples") {
     liveDataService.setPeoplesMarker(markerData);
     }
     else if (data.type == "auto") {
     liveDataService.setAutoMarker(markerData);
     }
     else if (data.type == "Loitering") {
     liveDataService.setLoiteringMarker(markerData);
     }
     else if (data.type == "Person of interest") {
     liveDataService.setPeopleOfInterestMarker(markerData);
     }*/
    liveDataService.setAllMarker(markerData);
    if ($scope.dataType === '' || $scope.dataType === data.type) {
      ib.open(map);
    }
  };
  /*function initMap() {
    map = new google.maps.Map(document.getElementById('liveMap'), {
      zoom: 13,
      center: {lat: 21.181272, lng: 72.835066},
      mapTypeId: google.maps.MapTypeId.TERRAIN
    });
    startcameraMarker();
  }*/
  /*initMap();*/
});
