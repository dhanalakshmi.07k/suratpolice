/**
 * Created by Suhas on 11/25/2015.
 */
suratCamControllers.controller('live', function ($scope, liveDataService, authInterceptor, settingService, $state) {

  $scope.dataType = "";
  var map;
  $scope.wantedPeoples = 1;
  $scope.alertType = function (type) {
    $scope.live.count.all = liveDataService.getAllAlertArrayLength() + 4;
    var allAlerts = liveDataService.getAllAlerts();
    var counter = 0;
    var myType = "";
    for (i = 0; i < allAlerts.length; i++) {
      myType = allAlerts[i].type;
      if (myType == type) {
        counter++
      }
    }
    $scope.live.count[type] = counter
    $scope.$apply();
  }
  $scope.addCamMarker = function (data) {
    /*$scope.toggleGroup($scope.dataType);*/
    var img;
    var num = 0, lat, log;
    img = "data:image/png;base64," + data.alertData['Photo'];
    num = "";
    star = " ";
    lat = data.alertData.Latitude;
    log = data.alertData.Longitude;
    var myOptions = {
      content: '<div class="markerMain  ' + data.type + '">' +
      '<img src="' + img + '" class="markerImg">' +
      '             <div class="markerContMain">' +
      '                 <div class="markerCont">' +
      '<div class="markerText">' +
      '                     <div class="num">' + num + '' + star + '</div>' +
      '                     <div class="txt">' + data.type + '</div>' +
      '</div>' +
      '                  </div>' +
      '             </div>' +
      '        </div>',
      pixelOffset: new google.maps.Size(-28, -30), // left upper corner of the label
      position: new google.maps.LatLng(lat, log),
      closeBoxURL: "",
      type: data.type
    };
    var ib = new InfoBox(myOptions);
    var markerData = {
      markers: ib,
      markerType: data.type
    }
    liveDataService.setAllMarker(markerData);
    ib.open(liveDataService.getLiveMapData());
  };
  function startcameraMarker() {
    var camera = [
      {
        alertData: {
          "title": 'camera1',
          "Latitude": '21.173690',
          "Longitude": '72.881950',
          "description": 'group of 300 auto',
          "Photo": "iVBORw0KGgoAAAANSUhEUgAAACMAAAAjCAYAAAAe2bNZAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAIGNIUk0AAHolAACAgwAA+f8AAIDoAABSCAABFVgAADqXAAAXb9daH5AAAAb1SURBVHjapJh9bFV3Gcc/z3m597b3lr7YUhAGg2nnuhhFI6uUxBg1m4Nuy5JFE9G4ZYE54wz/zKgzmQvZXya6JVtMiIAJyf5x2gELTHT7YxQbKpvRUkcZG0EmlLLS0va+9p7HP87L/Z1zbsPUk/See3/9nef5/p7X73OE00ukL0n81mBNe4HNIFtA+4F1IJ2gWUQqKNeBi6ATICdBTwFTcdHii/M/Elo/GpjbQR4GHkC0DxBUGkBFQFPCFWQSYRjV/cDZ/xdMF/BD4DHQlZEwCJSLabXE82oo5iror4HnEJn5X8B8AfSXwGDj5IbShqK0hczTx/bpCCK7UcaagbFofm1DOAwymHaZAUQ0AKdNrJoCAsggymGEbc2UNgOzHTiIam8kJHkI1cBIErhN4nGTvIfAfOC9KAdBtt8EjGxC2AfaEfM7gfIoPmiuOLZHmiRkuK4dCPsQ2dTYKzEw7cCLID2Np8ObIVyMmEmaTAVsG1osyFpgWc0rhO++HpQXA72hZSJku0EHokBtnCJwVbCuwbomMsuxoWAhtTL5M6doPT8OtoBrGUgCOVE26QDobiOb6gCfBD0JdMczITit6Z7wuwAe4FiQB3t+gfY3X6P98H6c9yYQy6a4dRtXvr+HJTcPS55hTTHj/RrKFuCcE2h+FKQ7Vj+igNR0OYksAdZihY5jh1kxfICWiTHUddFMC4Ky4tB+ap3dTD3+FMwnMis8tNIN+ijwI+Gteg9wAtW+WCqKNuIlDExPfEu0gF1cpO3EcToO7SN7ZgyxLbyWAmJZjaCulNF8gQvPHaG6+lYoe4aBY0VzEtjqoAyA3hYv6UZMKOApuKElqnS+doi2V35L7uxbIIIWVqAivmjPa7ggm8OamabryEGu7Hoqke4SHFgI9A84wCCInaqwYXjYviWcYpnCH4/SObyP7MQY2Dbakkcty38q3Zt8Edkcba+/zPSDO6l3rIRanVj9Egj0DzoI/TEgoYVsG1rBXijTfvwYHa8cIDNxCrUttNDuW0T97IjOGqyJBDFXLmItzFO6YzOaK/gWTvathqX6HZS1jSoeArGwqdJ2/Chdv/8NmXf+iloW2lqILBEqj/JLvUiuVMtQKlJfdQuz3/wB14Yexsu1Qs0z9CSaKrrWQehCjXR2bOxaiVue3UXuxKu+K1wXqdXw1EPEBs8zZAmoh5RLiG2jjkuto4cbDz3E3Ne/RXXdGqgDi2YR1QQQAbTLQjUbZYsIeIo6NpX1n0LdLFa5SG1jP3Pbd6C2C4s3/GANXCFeHcRi6rGnWfj8l1E3x7/3HGD6e09SXb0G94MPWfX8z2m9OO5XZbNYRu3GD3cHqKBmkfTwLJcrO39Cqe9OMh+8T+XW26FcZOGur7Jy77O4F/6J19aBLNWQ4jza2kbpE3eSvTCJItTWbvBlFSBz5l987KVfUf7cAMW+T8fTOlYDpeIAM6DrY3HjeWjJYvZr98NChY07v0L23b8zP/Rdrj6xh87f7aV15BhLq9Yy/+AucuNjbHjiftR1ubbzaVrPjpO5dB7N53Dff5davg3PzSR7k1FrAGXGAS4Bm1LUUT2YE8g5FL94N9l3TlM4+hKVNbdx6Wd7KfztL5RXr6O6YQMtZ/5BfnKcalcPzsxlPv7THb7FLAcVQR0HDZtmqoRExfWSAzIBOpSijBEom5l7v01mdprrd3+DhTvuQsXhxsCXYAmoKitef5m20T+BKJn3JvBsC7UdqFcQsf3vzQm+qXPCAR0B6qjaqQ0ClD2qveu5+OPn/eWS+k2vFvSsjMXsvd/BKi5gVRZZ/MygH9iqqONif3iZ/Kk3ENPqyWoPdZARB2QU9DzQl6KNKj79qisUNe7jML6qHpU1G7n85C/SDLQA+dHTrPjzH5BaNQ2kQUHOA6MWMA0MN59SNHE3+I25sVaHRc8HXFIoef7fDVhq66T42a0stRSMGiNJUMMg0waf4SRodyzfUpOILjOKmPsTbrYsLIp4ngPqpNmA6jVEtgDnQhp2DnghZZlmo0iSmJsPxZpl4Na6h0cr4JqZY7BHXgj0R0wv4MB6DGSgCZKEvw2Oo7L83hTA1DgzCtwDzCWngzngcT+GmliBxNCmNB9TIxAGdTUDPnKxTgf65gxCHhvE3kZ4BGQ2TsxvcklyNJGES5KYZRblEeDtmwxxegRhB8hUeo5PZFTIW2IE3owjSXtQmALdARy5+UTpP/wqMAQ60qgticwJ4yE5OMAyGSAgjKA6FMj/yLM2wBjIfYg8A3o1NaYk56r4+JEEdBXhGVTv8+Uu4+2mbyFiUwKg4fsZfQDoi1G8pgN+NF5M+gVV9iOcZRme/N+CCf/TC7IZ2AL0I7oOpRMkC1pB5DqqF/3mi/HmKlkQm1//GQDEQEbqJdBf6AAAAABJRU5ErkJggg=="
        },
        type: "camera"
      },
      {
        alertData: {
          "title": 'camera2',
          "Latitude": '21.157806',
          "Longitude": '72.794691',
          "description": 'group of 300 auto',
          "Photo": "iVBORw0KGgoAAAANSUhEUgAAACMAAAAjCAYAAAAe2bNZAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAIGNIUk0AAHolAACAgwAA+f8AAIDoAABSCAABFVgAADqXAAAXb9daH5AAAAb1SURBVHjapJh9bFV3Gcc/z3m597b3lr7YUhAGg2nnuhhFI6uUxBg1m4Nuy5JFE9G4ZYE54wz/zKgzmQvZXya6JVtMiIAJyf5x2gELTHT7YxQbKpvRUkcZG0EmlLLS0va+9p7HP87L/Z1zbsPUk/See3/9nef5/p7X73OE00ukL0n81mBNe4HNIFtA+4F1IJ2gWUQqKNeBi6ATICdBTwFTcdHii/M/Elo/GpjbQR4GHkC0DxBUGkBFQFPCFWQSYRjV/cDZ/xdMF/BD4DHQlZEwCJSLabXE82oo5iror4HnEJn5X8B8AfSXwGDj5IbShqK0hczTx/bpCCK7UcaagbFofm1DOAwymHaZAUQ0AKdNrJoCAsggymGEbc2UNgOzHTiIam8kJHkI1cBIErhN4nGTvIfAfOC9KAdBtt8EjGxC2AfaEfM7gfIoPmiuOLZHmiRkuK4dCPsQ2dTYKzEw7cCLID2Np8ObIVyMmEmaTAVsG1osyFpgWc0rhO++HpQXA72hZSJku0EHokBtnCJwVbCuwbomMsuxoWAhtTL5M6doPT8OtoBrGUgCOVE26QDobiOb6gCfBD0JdMczITit6Z7wuwAe4FiQB3t+gfY3X6P98H6c9yYQy6a4dRtXvr+HJTcPS55hTTHj/RrKFuCcE2h+FKQ7Vj+igNR0OYksAdZihY5jh1kxfICWiTHUddFMC4Ky4tB+ap3dTD3+FMwnMis8tNIN+ijwI+Gteg9wAtW+WCqKNuIlDExPfEu0gF1cpO3EcToO7SN7ZgyxLbyWAmJZjaCulNF8gQvPHaG6+lYoe4aBY0VzEtjqoAyA3hYv6UZMKOApuKElqnS+doi2V35L7uxbIIIWVqAivmjPa7ggm8OamabryEGu7Hoqke4SHFgI9A84wCCInaqwYXjYviWcYpnCH4/SObyP7MQY2Dbakkcty38q3Zt8Edkcba+/zPSDO6l3rIRanVj9Egj0DzoI/TEgoYVsG1rBXijTfvwYHa8cIDNxCrUttNDuW0T97IjOGqyJBDFXLmItzFO6YzOaK/gWTvathqX6HZS1jSoeArGwqdJ2/Chdv/8NmXf+iloW2lqILBEqj/JLvUiuVMtQKlJfdQuz3/wB14Yexsu1Qs0z9CSaKrrWQehCjXR2bOxaiVue3UXuxKu+K1wXqdXw1EPEBs8zZAmoh5RLiG2jjkuto4cbDz3E3Ne/RXXdGqgDi2YR1QQQAbTLQjUbZYsIeIo6NpX1n0LdLFa5SG1jP3Pbd6C2C4s3/GANXCFeHcRi6rGnWfj8l1E3x7/3HGD6e09SXb0G94MPWfX8z2m9OO5XZbNYRu3GD3cHqKBmkfTwLJcrO39Cqe9OMh+8T+XW26FcZOGur7Jy77O4F/6J19aBLNWQ4jza2kbpE3eSvTCJItTWbvBlFSBz5l987KVfUf7cAMW+T8fTOlYDpeIAM6DrY3HjeWjJYvZr98NChY07v0L23b8zP/Rdrj6xh87f7aV15BhLq9Yy/+AucuNjbHjiftR1ubbzaVrPjpO5dB7N53Dff5davg3PzSR7k1FrAGXGAS4Bm1LUUT2YE8g5FL94N9l3TlM4+hKVNbdx6Wd7KfztL5RXr6O6YQMtZ/5BfnKcalcPzsxlPv7THb7FLAcVQR0HDZtmqoRExfWSAzIBOpSijBEom5l7v01mdprrd3+DhTvuQsXhxsCXYAmoKitef5m20T+BKJn3JvBsC7UdqFcQsf3vzQm+qXPCAR0B6qjaqQ0ClD2qveu5+OPn/eWS+k2vFvSsjMXsvd/BKi5gVRZZ/MygH9iqqONif3iZ/Kk3ENPqyWoPdZARB2QU9DzQl6KNKj79qisUNe7jML6qHpU1G7n85C/SDLQA+dHTrPjzH5BaNQ2kQUHOA6MWMA0MN59SNHE3+I25sVaHRc8HXFIoef7fDVhq66T42a0stRSMGiNJUMMg0waf4SRodyzfUpOILjOKmPsTbrYsLIp4ngPqpNmA6jVEtgDnQhp2DnghZZlmo0iSmJsPxZpl4Na6h0cr4JqZY7BHXgj0R0wv4MB6DGSgCZKEvw2Oo7L83hTA1DgzCtwDzCWngzngcT+GmliBxNCmNB9TIxAGdTUDPnKxTgf65gxCHhvE3kZ4BGQ2TsxvcklyNJGES5KYZRblEeDtmwxxegRhB8hUeo5PZFTIW2IE3owjSXtQmALdARy5+UTpP/wqMAQ60qgticwJ4yE5OMAyGSAgjKA6FMj/yLM2wBjIfYg8A3o1NaYk56r4+JEEdBXhGVTv8+Uu4+2mbyFiUwKg4fsZfQDoi1G8pgN+NF5M+gVV9iOcZRme/N+CCf/TC7IZ2AL0I7oOpRMkC1pB5DqqF/3mi/HmKlkQm1//GQDEQEbqJdBf6AAAAABJRU5ErkJggg=="
        },
        type: "camera"
      },
      {
        alertData: {
          "title": 'camera3',
          "Latitude": '21.197917',
          "Longitude": '72.838573',
          "description": 'group of 300 auto',
          "Photo": "iVBORw0KGgoAAAANSUhEUgAAACMAAAAjCAYAAAAe2bNZAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAIGNIUk0AAHolAACAgwAA+f8AAIDoAABSCAABFVgAADqXAAAXb9daH5AAAAb1SURBVHjapJh9bFV3Gcc/z3m597b3lr7YUhAGg2nnuhhFI6uUxBg1m4Nuy5JFE9G4ZYE54wz/zKgzmQvZXya6JVtMiIAJyf5x2gELTHT7YxQbKpvRUkcZG0EmlLLS0va+9p7HP87L/Z1zbsPUk/See3/9nef5/p7X73OE00ukL0n81mBNe4HNIFtA+4F1IJ2gWUQqKNeBi6ATICdBTwFTcdHii/M/Elo/GpjbQR4GHkC0DxBUGkBFQFPCFWQSYRjV/cDZ/xdMF/BD4DHQlZEwCJSLabXE82oo5iror4HnEJn5X8B8AfSXwGDj5IbShqK0hczTx/bpCCK7UcaagbFofm1DOAwymHaZAUQ0AKdNrJoCAsggymGEbc2UNgOzHTiIam8kJHkI1cBIErhN4nGTvIfAfOC9KAdBtt8EjGxC2AfaEfM7gfIoPmiuOLZHmiRkuK4dCPsQ2dTYKzEw7cCLID2Np8ObIVyMmEmaTAVsG1osyFpgWc0rhO++HpQXA72hZSJku0EHokBtnCJwVbCuwbomMsuxoWAhtTL5M6doPT8OtoBrGUgCOVE26QDobiOb6gCfBD0JdMczITit6Z7wuwAe4FiQB3t+gfY3X6P98H6c9yYQy6a4dRtXvr+HJTcPS55hTTHj/RrKFuCcE2h+FKQ7Vj+igNR0OYksAdZihY5jh1kxfICWiTHUddFMC4Ky4tB+ap3dTD3+FMwnMis8tNIN+ijwI+Gteg9wAtW+WCqKNuIlDExPfEu0gF1cpO3EcToO7SN7ZgyxLbyWAmJZjaCulNF8gQvPHaG6+lYoe4aBY0VzEtjqoAyA3hYv6UZMKOApuKElqnS+doi2V35L7uxbIIIWVqAivmjPa7ggm8OamabryEGu7Hoqke4SHFgI9A84wCCInaqwYXjYviWcYpnCH4/SObyP7MQY2Dbakkcty38q3Zt8Edkcba+/zPSDO6l3rIRanVj9Egj0DzoI/TEgoYVsG1rBXijTfvwYHa8cIDNxCrUttNDuW0T97IjOGqyJBDFXLmItzFO6YzOaK/gWTvathqX6HZS1jSoeArGwqdJ2/Chdv/8NmXf+iloW2lqILBEqj/JLvUiuVMtQKlJfdQuz3/wB14Yexsu1Qs0z9CSaKrrWQehCjXR2bOxaiVue3UXuxKu+K1wXqdXw1EPEBs8zZAmoh5RLiG2jjkuto4cbDz3E3Ne/RXXdGqgDi2YR1QQQAbTLQjUbZYsIeIo6NpX1n0LdLFa5SG1jP3Pbd6C2C4s3/GANXCFeHcRi6rGnWfj8l1E3x7/3HGD6e09SXb0G94MPWfX8z2m9OO5XZbNYRu3GD3cHqKBmkfTwLJcrO39Cqe9OMh+8T+XW26FcZOGur7Jy77O4F/6J19aBLNWQ4jza2kbpE3eSvTCJItTWbvBlFSBz5l987KVfUf7cAMW+T8fTOlYDpeIAM6DrY3HjeWjJYvZr98NChY07v0L23b8zP/Rdrj6xh87f7aV15BhLq9Yy/+AucuNjbHjiftR1ubbzaVrPjpO5dB7N53Dff5davg3PzSR7k1FrAGXGAS4Bm1LUUT2YE8g5FL94N9l3TlM4+hKVNbdx6Wd7KfztL5RXr6O6YQMtZ/5BfnKcalcPzsxlPv7THb7FLAcVQR0HDZtmqoRExfWSAzIBOpSijBEom5l7v01mdprrd3+DhTvuQsXhxsCXYAmoKitef5m20T+BKJn3JvBsC7UdqFcQsf3vzQm+qXPCAR0B6qjaqQ0ClD2qveu5+OPn/eWS+k2vFvSsjMXsvd/BKi5gVRZZ/MygH9iqqONif3iZ/Kk3ENPqyWoPdZARB2QU9DzQl6KNKj79qisUNe7jML6qHpU1G7n85C/SDLQA+dHTrPjzH5BaNQ2kQUHOA6MWMA0MN59SNHE3+I25sVaHRc8HXFIoef7fDVhq66T42a0stRSMGiNJUMMg0waf4SRodyzfUpOILjOKmPsTbrYsLIp4ngPqpNmA6jVEtgDnQhp2DnghZZlmo0iSmJsPxZpl4Na6h0cr4JqZY7BHXgj0R0wv4MB6DGSgCZKEvw2Oo7L83hTA1DgzCtwDzCWngzngcT+GmliBxNCmNB9TIxAGdTUDPnKxTgf65gxCHhvE3kZ4BGQ2TsxvcklyNJGES5KYZRblEeDtmwxxegRhB8hUeo5PZFTIW2IE3owjSXtQmALdARy5+UTpP/wqMAQ60qgticwJ4yE5OMAyGSAgjKA6FMj/yLM2wBjIfYg8A3o1NaYk56r4+JEEdBXhGVTv8+Uu4+2mbyFiUwKg4fsZfQDoi1G8pgN+NF5M+gVV9iOcZRme/N+CCf/TC7IZ2AL0I7oOpRMkC1pB5DqqF/3mi/HmKlkQm1//GQDEQEbqJdBf6AAAAABJRU5ErkJggg=="
        },
        type: "camera"
      },
      {
        alertData: {
          "title": 'camera4',
          "Latitude": '21.179074',
          "Longitude": '72.801331',
          "description": 'group of 300 auto',
          "Photo": "iVBORw0KGgoAAAANSUhEUgAAACMAAAAjCAYAAAAe2bNZAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAIGNIUk0AAHolAACAgwAA+f8AAIDoAABSCAABFVgAADqXAAAXb9daH5AAAAb1SURBVHjapJh9bFV3Gcc/z3m597b3lr7YUhAGg2nnuhhFI6uUxBg1m4Nuy5JFE9G4ZYE54wz/zKgzmQvZXya6JVtMiIAJyf5x2gELTHT7YxQbKpvRUkcZG0EmlLLS0va+9p7HP87L/Z1zbsPUk/See3/9nef5/p7X73OE00ukL0n81mBNe4HNIFtA+4F1IJ2gWUQqKNeBi6ATICdBTwFTcdHii/M/Elo/GpjbQR4GHkC0DxBUGkBFQFPCFWQSYRjV/cDZ/xdMF/BD4DHQlZEwCJSLabXE82oo5iror4HnEJn5X8B8AfSXwGDj5IbShqK0hczTx/bpCCK7UcaagbFofm1DOAwymHaZAUQ0AKdNrJoCAsggymGEbc2UNgOzHTiIam8kJHkI1cBIErhN4nGTvIfAfOC9KAdBtt8EjGxC2AfaEfM7gfIoPmiuOLZHmiRkuK4dCPsQ2dTYKzEw7cCLID2Np8ObIVyMmEmaTAVsG1osyFpgWc0rhO++HpQXA72hZSJku0EHokBtnCJwVbCuwbomMsuxoWAhtTL5M6doPT8OtoBrGUgCOVE26QDobiOb6gCfBD0JdMczITit6Z7wuwAe4FiQB3t+gfY3X6P98H6c9yYQy6a4dRtXvr+HJTcPS55hTTHj/RrKFuCcE2h+FKQ7Vj+igNR0OYksAdZihY5jh1kxfICWiTHUddFMC4Ky4tB+ap3dTD3+FMwnMis8tNIN+ijwI+Gteg9wAtW+WCqKNuIlDExPfEu0gF1cpO3EcToO7SN7ZgyxLbyWAmJZjaCulNF8gQvPHaG6+lYoe4aBY0VzEtjqoAyA3hYv6UZMKOApuKElqnS+doi2V35L7uxbIIIWVqAivmjPa7ggm8OamabryEGu7Hoqke4SHFgI9A84wCCInaqwYXjYviWcYpnCH4/SObyP7MQY2Dbakkcty38q3Zt8Edkcba+/zPSDO6l3rIRanVj9Egj0DzoI/TEgoYVsG1rBXijTfvwYHa8cIDNxCrUttNDuW0T97IjOGqyJBDFXLmItzFO6YzOaK/gWTvathqX6HZS1jSoeArGwqdJ2/Chdv/8NmXf+iloW2lqILBEqj/JLvUiuVMtQKlJfdQuz3/wB14Yexsu1Qs0z9CSaKrrWQehCjXR2bOxaiVue3UXuxKu+K1wXqdXw1EPEBs8zZAmoh5RLiG2jjkuto4cbDz3E3Ne/RXXdGqgDi2YR1QQQAbTLQjUbZYsIeIo6NpX1n0LdLFa5SG1jP3Pbd6C2C4s3/GANXCFeHcRi6rGnWfj8l1E3x7/3HGD6e09SXb0G94MPWfX8z2m9OO5XZbNYRu3GD3cHqKBmkfTwLJcrO39Cqe9OMh+8T+XW26FcZOGur7Jy77O4F/6J19aBLNWQ4jza2kbpE3eSvTCJItTWbvBlFSBz5l987KVfUf7cAMW+T8fTOlYDpeIAM6DrY3HjeWjJYvZr98NChY07v0L23b8zP/Rdrj6xh87f7aV15BhLq9Yy/+AucuNjbHjiftR1ubbzaVrPjpO5dB7N53Dff5davg3PzSR7k1FrAGXGAS4Bm1LUUT2YE8g5FL94N9l3TlM4+hKVNbdx6Wd7KfztL5RXr6O6YQMtZ/5BfnKcalcPzsxlPv7THb7FLAcVQR0HDZtmqoRExfWSAzIBOpSijBEom5l7v01mdprrd3+DhTvuQsXhxsCXYAmoKitef5m20T+BKJn3JvBsC7UdqFcQsf3vzQm+qXPCAR0B6qjaqQ0ClD2qveu5+OPn/eWS+k2vFvSsjMXsvd/BKi5gVRZZ/MygH9iqqONif3iZ/Kk3ENPqyWoPdZARB2QU9DzQl6KNKj79qisUNe7jML6qHpU1G7n85C/SDLQA+dHTrPjzH5BaNQ2kQUHOA6MWMA0MN59SNHE3+I25sVaHRc8HXFIoef7fDVhq66T42a0stRSMGiNJUMMg0waf4SRodyzfUpOILjOKmPsTbrYsLIp4ngPqpNmA6jVEtgDnQhp2DnghZZlmo0iSmJsPxZpl4Na6h0cr4JqZY7BHXgj0R0wv4MB6DGSgCZKEvw2Oo7L83hTA1DgzCtwDzCWngzngcT+GmliBxNCmNB9TIxAGdTUDPnKxTgf65gxCHhvE3kZ4BGQ2TsxvcklyNJGES5KYZRblEeDtmwxxegRhB8hUeo5PZFTIW2IE3owjSXtQmALdARy5+UTpP/wqMAQ60qgticwJ4yE5OMAyGSAgjKA6FMj/yLM2wBjIfYg8A3o1NaYk56r4+JEEdBXhGVTv8+Uu4+2mbyFiUwKg4fsZfQDoi1G8pgN+NF5M+gVV9iOcZRme/N+CCf/TC7IZ2AL0I7oOpRMkC1pB5DqqF/3mi/HmKlkQm1//GQDEQEbqJdBf6AAAAABJRU5ErkJggg=="
        },
        type: "camera"
      }
    ];
    for (var j = 0; j < camera.length; j++) {
      $scope.addCamMarker(camera[j]);
    }
  };
  function initMap() {
    map = new google.maps.Map(document.getElementById('liveMap'), {
      zoom: 13,
      center: {lat: 21.181272, lng: 72.835066},
      mapTypeId: google.maps.MapTypeId.TERRAIN
    });
    liveDataService.setLiveMapObject(map)
    startcameraMarker();
  }
  initMap();
  $scope.toggleGroup = function (type) {
    $scope.dataType=type;
    $scope.$parent.filterType=type;
    liveDataService.setLiveMapFilter(type);
    if (type === '') {
      for (var i = 0; i < liveDataService.getAllMarker().length; i++) {
        var marker = liveDataService.getAllMarker()[i].markers;
        marker.setVisible(true);
        /*marker.open(map);*/
      }
    }
    else {
      for (var i = 0; i < liveDataService.getAllMarker().length; i++) {
        var marker = liveDataService.getAllMarker()[i].markers;
        var markerType = liveDataService.getAllMarker()[i].markerType;
        if (type === markerType) {
          marker.setVisible(true);
        }
        else {
          marker.setVisible(false);
        }
      }
    }
  };
  $scope.clearNotificationData = function () {
    settingService.cancelAllSimulation();
    settingService.setAllSimulatorFlag(false);
    for (var i = 0; i < liveDataService.getAllMarker().length; i++) {
      var marker = liveDataService.getAllMarker()[i].markers;
      marker.close();
    }
    liveDataService.clearAllData();
    $scope.toggleGroup('');
    $scope.clearCount();
    $scope.notifyMessage = [];

  }
  $scope.clearCount = function () {
    $scope.live = {
      count: {
        all: 4,
        loitering: 0,
        wanted: 0,
        auto: 0,
        peoples: 0,
        street_camera: 0,
        body_camera: 0
      }
    }
  }

  $scope.focusOn = function(){
  }
  $scope.drawExistingMarkerOnMap = function(){
    $scope.dataType='';
    $scope.$parent.filterType='';
    for (var i = 0; i < liveDataService.getAllMarker().length; i++) {
      var marker = liveDataService.getAllMarker()[i].markers;
      marker.setVisible(true);
      marker.open(map);
    }
  }
  $scope.drawExistingMarkerOnMap();

});
