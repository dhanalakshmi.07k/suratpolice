/**
 * Created by Suhas on 11/25/2015.
 */
suratCamControllers.controller('analytics', function ($scope,analyticsService) {
  $scope.xAxis=['26 Oct','27 Oct','28 Oct','29 Oct','30 Oct'];
  $scope.yAxis=[80,70,40,50,80];
  $scope.start=2;
  $scope.end=6;
  Array.prototype.SumArray = function (arr) {
    var sum = [];
    if (arr != null && this.length == arr.length) {
      for (var i = 0; i < arr.length; i++) {
        sum.push(this[i] + arr[i]);
      }
    }
    return sum;
  }
  Array.prototype.AvgArray = function (length) {
    var avgArray = [];
    if (this.length != null) {
      for (var i = 0; i < this.length; i++) {
        this[i]= this[i] / length;
        avgArray.push(this[i]);
      }
    }
    return avgArray;
  }
  $scope.changeHighChartValue = function(){
    /*var val = ['one','two','three'];
     var currVal = val[Math.floor(Math.random()*val.length)];*/
    analyticsService.getHIghChartValForGivenRange($scope.start,$scope.end).then(function(result){
      var highChartValArrray = result.data;
      var average_array = [];
      var result_array = [];
      var array1 = [];
      for(var i =0;i<highChartValArrray.length;i++){
        if(i==0){
          if(highChartValArrray[i+1]){
            average_array = highChartValArrray[i+1].val;
          }else{
            average_array = highChartValArrray[i].val;
          }

        }
        if(i==highChartValArrray.length-1){
          result_array = average_array.AvgArray(highChartValArrray.length)
          $scope.changeHighVal(result_array);
        }
        array1 = highChartValArrray[i].val;
        average_array = average_array.SumArray(array1);
      }

    });
  }
  $scope.changeHighVal = function(series_array){
    var score = series_array;
    var categories=['25 OCT', '26 OCT', '27 OCT', '28 OCT', '29 OCT', '30 OCT']
    $scope.renderChart = {
      chart: {
        type: 'areaspline'
      },
      legend: {
        enabled: false
      },
      title: {
        text: ''
      },
      yAxis: {
        title: {
          text: 'Occupancy',
          align: 'low'
        },
        floor: 0,
        /*ceiling: 100,*/
        gridLineDashStyle: 'Dot',
        gridLineWidth: 1,
        gridLineColor: '#719baf'
      },
      xAxis: {
        title: {
          text: 'Days',
          align: 'high'
        },
        categories:categories
      },
      tooltip: {
        shared: true,
        valueSuffix: ' units'
      },
      credits: {
        enabled: false
      },
      plotOptions: {
        areaspline: {
          fillOpacity: 0.7
        },
        series: {
          lineColor: '#525b62',
          lineWidth: 1,
          marker: {
            fillColor: '#FFFFFF',
            lineWidth: 1,
            lineColor: '#525b62'
          }
        }

      },
      series: [{
        data: score,
        color: '#f6d5d7',
        states: {
          hover: {
            enabled: false
          }
        }
      }]
    };
  };
  $scope.changeHighChartValue();


  var incPoint=2;
  var decPoint=-3;
  var incSolidGauge= function (ele) {
    if(incPoint>30){
      incPoint=5;
    }
    incPoint=incPoint+3;
    var solidGauge=$(ele).find(".solidGauge").highcharts();
    var point = solidGauge.series[0].points[0];
    point.update(incPoint);
  };
  var decSolidGauge= function (ele) {
    if(decPoint<-30){
      decPoint=-5;
    }
    decPoint=decPoint-2;
    var solidGauge=$(ele).find(".solidGauge").highcharts();
    var point = solidGauge.series[0].points[0];
    point.update(decPoint);
  };
  setInterval(function () {
    incSolidGauge(".loitering");
    decSolidGauge(".traffic");
    decSolidGauge(".accidents");
  },10000)


});
