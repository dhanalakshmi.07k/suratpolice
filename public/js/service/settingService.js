/**
 * Created by SUhas on 11/26/2015.
 */
suratPoliceServices.factory("settingService", function ($http) {
  var notificationSimulator;
  var notificationSpecificSimulator;
  var notificationLoitertingSimulator;
  var notificationPersonOfInsterestSimulator;
  var notificationAutoSimulator;
  var notificationCrowdSimulator;
  var currentSimulationSetting = {
    isNotificationSimulation: false,
    isWantedNotificationSimulation: false,
    isAutoNotificationSimulation: false,
    isMobNotificationSimulation: false,
    isLoteringSimulation: false,
    notificationSimulationInterval: 5000
  }
  return {
    getNotificationSimulation: function () {
      return currentSimulationSetting.isNotificationSimulation
    },
    getLoteringNotificationSimulation: function () {
      return currentSimulationSetting.isLoteringSimulation
    },
    getWantedNotificationSimulation: function () {
      return currentSimulationSetting.isWantedNotificationSimulation
    },
    getAutoNotificationSimulation: function () {
      return currentSimulationSetting.isAutoNotificationSimulation
    },
    getMobNotificationSimulation: function () {
      return currentSimulationSetting.isMobNotificationSimulation
    },
    setNotificationSimulation: function (val) {
      currentSimulationSetting.isNotificationSimulation = val;
    },
    setLoteringNotificationSimulation: function (val) {
      currentSimulationSetting.isLoteringSimulation = val;
    },
    setWantedNotificationSimulation: function (val) {
      currentSimulationSetting.isWantedNotificationSimulation = val;
    },
    setAutoNotificationSimulation: function (val) {
      currentSimulationSetting.isAutoNotificationSimulation = val;
    },
    setMobNotificationSimulation: function (val) {
      currentSimulationSetting.isMobNotificationSimulation = val;
    },
    startNotificationSimulation: function (flag) {
      var alertType = ['loitering', 'autoCrowd', 'wanted', 'mobCrowd'];
      notificationSimulator = setInterval(function () {
        var type = alertType[Math.floor(Math.random() * alertType.length)];
        if (type == 'loitering') {
          $http.get('/loiteringNotifySimulator').then(function (result) {
          });
        }
        else if (type == 'wanted') {
          $http.get('/wantedNotifySimulator').then(function (result) {
          });
        }
        else if (type == 'autoCrowd') {
          $http.get('/autoNotifySimulator').then(function (result) {
          });
        }
        else if (type == 'mobCrowd') {
          $http.get('/peopleNotifySimulator').then(function (result) {
          });
        }
      }, currentSimulationSetting.notificationSimulationInterval);
    },
    cancelNotificationSimulation: function (flag) {
      clearInterval(notificationSimulator);
    },
    startSpecificSimulation: function (flag, simulatorType) {
      notificationSpecificSimulator = setInterval(function () {
        console.log(simulatorType);
        var type = simulatorType;
        if (type == 'loitering') {
          $http.get('/loiteringNotifySimulator').then(function (result) {
          });
        }
        else if (type == 'wanted') {
          $http.get('/wantedNotifySimulator').then(function (result) {
          });
        }
        else if (type == 'autoCrowd') {
          $http.get('/autoNotifySimulator').then(function (result) {
          });
        }
        else if (type == 'mobCrowd') {
          $http.get('/peopleNotifySimulator').then(function (result) {
          });
        }
      }, currentSimulationSetting.notificationSimulationInterval);
    },
    cancelSpecificSimulation: function (flag) {
      clearInterval(notificationSpecificSimulator);
    },
    startLoiteringSimulation: function (flag) {
      notificationLoitertingSimulator = setInterval(function () {
        $http.get('/loiteringNotifySimulator').then(function (result) {
        });
      }, currentSimulationSetting.notificationSimulationInterval);
    },
    cancelLoiteringSimulation: function (flag) {
      clearInterval(notificationLoitertingSimulator);
    },
    startPersonOfInterestSimulation: function (flag) {
      notificationPersonOfInsterestSimulator = setInterval(function () {
        $http.get('/wantedNotifySimulator').then(function (result) {
        });
      }, currentSimulationSetting.notificationSimulationInterval);
    },
    cancelPersonOfInterestSimulation: function (flag) {
      clearInterval(notificationPersonOfInsterestSimulator);
    },
    startAutoSimulation: function (flag) {
      notificationAutoSimulator = setInterval(function () {
        $http.get('/autoNotifySimulator').then(function (result) {
        });
      }, currentSimulationSetting.notificationSimulationInterval);
    },
    cancelAutoSimulation: function (flag) {
      clearInterval(notificationAutoSimulator);
    },
    startCrowdSimulation: function (flag) {
      notificationCrowdSimulator = setInterval(function () {
        $http.get('/peopleNotifySimulator').then(function (result) {
        });
      }, currentSimulationSetting.notificationSimulationInterval);
    },
    cancelCrowdSimulation: function (flag) {
      clearInterval(notificationCrowdSimulator);
    },
    cancelAllSimulation: function () {
      clearInterval(notificationCrowdSimulator);
      clearInterval(notificationAutoSimulator);
      clearInterval(notificationPersonOfInsterestSimulator);
      clearInterval(notificationLoitertingSimulator);
      clearInterval(notificationSimulator);
    },
    setAllSimulatorFlag: function (val) {
      currentSimulationSetting.isNotificationSimulation = val;
      currentSimulationSetting.isNotificationLoteringSimulation = val;
      currentSimulationSetting.isWantedNotificationSimulation = val;
      currentSimulationSetting.isAutoNotificationSimulation = val;
      currentSimulationSetting.isMobNotificationSimulation = val;
      currentSimulationSetting.isLoteringSimulation = val;
    }
  }
})
