/**
 * Created by Suhas on 12/4/2015.
 */
suratPoliceServices.factory("liveDataService", function ($http) {
    var livedata = {
      allMapArray: [],
      loiteringArray: [],
      personOfInterestArray: [],
      autoCrowdArray: [],
      crowdArray: [],
      bodyCameraArray: [],
      streetCameraArray: []
    }
    var liveMarkerData = {
      allMapArray: [],
      loiteringArray: [],
      personOfInterestArray: [],
      autoCrowdArray: [],
      crowdArray: [],
      bodyCameraArray: [],
      streetCameraArray: []
    }
    var mapData = {
      liveMapData:''
    };
    var mapFilterType = {
      liveMapFilter:''
    }
    return {
      getAllAlerts: function () {
        return livedata.allMapArray;
      },
      getAllAlertArrayLength: function () {
        return livedata.allMapArray.length;
      },
      setAllAlerts: function (alertObj) {
        livedata.allMapArray.unshift(alertObj)
      },
      getLoiteringAlerts: function () {
        return livedata.loiteringArray;
      },
      getLoiteringAlertArrayLength: function () {
        return livedata.loiteringArray.length;
      },
      setLoiteringAlerts: function (alertObj) {
        livedata.loiteringArray.unshift(alertObj)
        console.log("hiiih")
      },
      getPersonOfInterestAlerts: function () {
        return livedata.personOfInterestArray;
      },
      getPersonOfInterestAlertArrayLength: function () {
        return livedata.personOfInterestArray.length;
      },
      setPersonOfInterestAlerts: function (alertObj) {
        livedata.personOfInterestArray.unshift(alertObj)
      },
      getAutoCrowdAlerts: function () {
        return livedata.autoCrowdArray;
      },
      getAutoCrowdAlertArrayLength: function () {
        return livedata.autoCrowdArray.length;
      },
      setAutoCrowdAlerts: function (alertObj) {
        livedata.autoCrowdArray.unshift(alertObj)
      },
      getCrowdAlerts: function () {
        return livedata.crowdArray;
      },
      getCrowdAlertArrayLength: function () {
        return livedata.crowdArray.length;
      },
      setCrowdAlerts: function (alertObj) {
        livedata.crowdArray.unshift(alertObj)
      },
      /*getBodyCameraAlerts: function () {
        return livedata.bodyCameraArray;
      },
      getBodyCameraAlertArrayLength: function () {
        return livedata.bodyCameraArray.length;
      },
      setBodyCameraAlerts: function (alertObj) {
        livedata.bodyCameraArray.unshift(alertObj)
      },
      getStreetCameraAlerts: function () {
        return livedata.streetCameraArray;
      },
      getStreetCameraAlertArrayLength: function () {
        return livedata.streetCameraArray.length;
      },
      setStreetCameraAlerts: function (alertObj) {
        livedata.streetCameraArray.unshift(alertObj)
      },*/
      clearAllData: function () {
        livedata.allMapArray = [];
        livedata.loiteringArray = [];
        livedata.personOfInterestArray = [];
        livedata.autoCrowdArray = [];
        livedata.crowdArray = [];
        livedata.bodyCameraArray = [];
        livedata.streetCameraArray = [];
        liveMarkerData.allMapArray = [];
        liveMarkerData.loiteringArray = [];
        liveMarkerData.personOfInterestArray = [];
        liveMarkerData.autoCrowdArray = [];
        liveMarkerData.crowdArray = [];
        liveMarkerData.bodyCameraArray = [];
        liveMarkerData.streetCameraArray = [];
      },
      setAllMarker: function (markerObject) {
        liveMarkerData.allMapArray.unshift(markerObject);
      },
      getAllMarker: function () {
        return liveMarkerData.allMapArray;
      },
      setLiveMapObject:function(obj){
        mapData.liveMapData = obj;
      },
      getLiveMapData:function(){
        return mapData.liveMapData;
      },
      /*setAutoMarker: function (markerObject) {
        liveMarkerData.autoCrowdArray.unshift(markerObject)
      },
      setPeoplesMarker: function (markerObject) {
        liveMarkerData.crowdArray.unshift(markerObject)
      },
      setPeopleOfInterestMarker: function (markerObject) {
        liveMarkerData.personOfInterestArray.unshift(markerObject)
      },
      setLoiteringMarker: function (markerObject) {
        liveMarkerData.loiteringArray.unshift(markerObject)
      },
      getLoiteringMarker: function () {
        return liveMarkerData.loiteringArray;
      },
      getPersonOfInterestMarker: function () {
        return liveMarkerData.personOfInterestArray;
      },
      getAutoMarker: function () {
        return liveMarkerData.autoCrowdArray;
      },
      getPeoplesMarker: function () {
        return liveMarkerData.crowdArray;
      }*/
      setLiveMapFilter:function(obj){
        mapFilterType.liveMapFilter = obj;
      },
      getLiveMapFilter:function(){
        return mapFilterType.liveMapFilter;
      }
    }
  }
)
