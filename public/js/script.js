$(document).ready(function () {
    var parentEle=$("#appWrapper");
    fun={
        dropDown: function () {
            parentEle.on("click",".dropDown .head",function (e) {
                e.stopPropagation();
                var init= $(this).data("init");
                if(init=="0"){
                    $(this).parent().find(".list").slideDown(300);
                    $(this).data({
                        "init":"1"
                    })
                }
                else{
                    $(this).parent().find(".list").slideUp(300);
                    $(this).data({
                        "init":"0"
                    })
                }
            });
           parentEle.on("click",".dropDown .list li",function (e) {
               var value=$(this).text();
               $(this).parents().eq(1).find(".headTitle").text(value);
            })
        },
        defaultClick: function () {
            $(document).click(function () {
                $(".dropDown .list").slideUp(300);
                $(".dropDown .head").data({
                    "init":"0"
                });
            })
        }

    };

    fun.dropDown();
    fun.defaultClick();

});