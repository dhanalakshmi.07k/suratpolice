/**
 * Created by MohammedSaleem on 11/11/15.
 */


(function($) {

    $.fn.dateRangeSlider = function(width) {

        this.each( function() {
            var slider = $(this).find(".rangeSlider");
            var sliderWidth = slider.width();
            var container = slider.find(".ContWrapper");

            var label = container.find(".label");
            var labelWidth = label.first().width();
            var totalLabels = container.find(".label").length;

            var containerWidth = labelWidth * totalLabels;
            var maxSlideVal = containerWidth - sliderWidth;


            container.css({
                width: containerWidth
            });

            var selector= container.find(".selector");

            selector.css({
                width: labelWidth*width,
                left: labelWidth
            });

            container.draggable({
                axis: "x",
                stop: function (event, ui) {
                    container = $(this);
                    var leftVal = container.position().left;
                    if (leftVal > 0) {
                        container.animate({
                            left: 0
                        }, 400)
                    }
                    else if (leftVal < -maxSlideVal) {
                        container.animate({
                            left: -maxSlideVal
                        }, 400)
                    }
                    //console.log(leftVal)
                }
            });


            var firstLabel;
            var lastLabel;

            function showSelection(){
                var selectorLeftVal=Math.round(selector.position().left);
                var selectorWidth= selector.outerWidth();

                    console.log(selectorLeftVal+" "+selectorWidth);


                firstLabel= selectorLeftVal/labelWidth;
                lastLabel=(selectorLeftVal+selectorWidth)/labelWidth;

                label.removeClass("selected");

                for(var i=firstLabel;i<lastLabel;i++){
                    label.eq(i).addClass("selected");
                }

            }
            showSelection();


            function resizing(){
                selector.resizable({
                    handles: "e, w",
                    distance: 5,
                    grid: [ labelWidth, 0 ],
                    containment: "parent",
                    resize: function( event, ui ) {
                        showSelection();
                        console.log(firstLabel+" "+lastLabel)
                    }
                });
            }
            resizing();


            $(this).bind("click", function(event, data) {


            });
        });
    };

    $.fn.dateSelectSlider = function() {

        this.each( function() {
            var slider = $(this).find(".selectSlider");
            var sliderWidth = slider.width();
            var container = slider.find(".ContWrapper");

            var label = container.find(".label");
            var labelWidth = label.first().width();
            var totalLabels = container.find(".label").length;

            var containerWidth = labelWidth * totalLabels;
            var maxSlideVal = containerWidth - sliderWidth;


            container.css({
                width: containerWidth
            });

            container.draggable({
                axis: "x",
                stop: function (event, ui) {
                    container = $(this);
                    var leftVal = container.position().left;
                    if (leftVal > 0) {
                        container.animate({
                            left: 0
                        }, 400)
                    }
                    else if (leftVal < -maxSlideVal) {
                        container.animate({
                            left: -maxSlideVal
                        }, 400)
                    }
                    //console.log(leftVal)
                }
            });

            label.click(function () {
                container.find(".label").removeClass("selected");
                $(this).addClass("selected");
            })



        });

    };

    $.fn.timeSlider = function() {

        this.each( function() {
            var slider = $(this).find(".hourSlider");
            var sliderWidth = slider.width();
            var container = slider.find(".ContWrapper");

            var label = container.find(".label");
            var labelWidth = label.first().width();
            var totalLabels = container.find(".label").length;

            var containerWidth = labelWidth * totalLabels;
            var maxSlideVal = containerWidth - sliderWidth;


            container.css({
                width: containerWidth
            });

            container.draggable({
                axis: "x",
                stop: function (event, ui) {
                    container = $(this);
                    var leftVal = container.position().left;
                    if (leftVal > 0) {
                        container.animate({
                            left: 0
                        }, 400)
                    }
                    else if (leftVal < -maxSlideVal) {
                        container.animate({
                            left: -maxSlideVal
                        }, 400)
                    }
                    //console.log(leftVal)
                }
            });

        });

    }

}(jQuery));



