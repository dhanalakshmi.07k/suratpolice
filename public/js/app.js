/**
 * Created by MohammedSaleem on 11/11/15.
 */

var dependency=['ui.router', 'verticalNav', 'appNotification', 'appMap', 'appSlider','suratCamControllers','areaGraph','circleGauge','suratPoliceServices'];
var suratCam = angular.module("suratCam", dependency);

suratCam.run(function ($rootScope) {
    $rootScope.baseUrl = 'full';
});


suratCam.config(function ($stateProvider, $urlRouterProvider) {
    $stateProvider.
    state('/login', {
        url: "/login",
        templateUrl: 'Unsecure/login.html',
        controller: 'loginCtrl'
    }).
      state('/signUp', {
        url: "/signUp",
        templateUrl: 'Unsecure/signUp.html',
        controller: 'loginCtrl'
      }).
    state('map', {
        url: "/map",
        templateUrl: 'templates/areaMap.html'
        /*,controller: 'notificationCtrl'*/
    }).
    state('map.live', {
        url: "/live",
        templateUrl: 'templates/live.html',
        controller: 'live',
        deepStateRedirect: true
    }).
    state('map.fence', {
        url: "/fence",
        templateUrl: 'templates/geoFence.html',
        controller: 'geoFence'
        /*controller: 'live',*/
    }).
    state('map.traffic', {
        url: "/traffic",
        templateUrl: 'templates/traffic.html',
        controller: 'traffic'
    }).
    state('content', {
        url: "/content",
        templateUrl: 'templates/contentBox.html'
    }).
    state('content.analytics', {
        url: "/analytics",
        templateUrl: 'templates/analytics.html',
        controller: 'analytics'
    }).
    state('content.settings', {
        url: "/settings",
        templateUrl: 'templates/settings.html'
    }).
    state('content.settings.conditions', {
        url: "/conditions",
        templateUrl: 'templates/settings/conditions.html'
    }).
    state('content.settings.conditions.alerts', {
        url: "/alerts/all",
        templateUrl: 'templates/settings/alerts.html'
    }).
    state('content.settings.conditions.simulator', {
      url: "/simulator/all",
      templateUrl: 'templates/settings/simulator.html'
    }).
    state('content.settings.conditions.newAlert', {
        url: "/alerts/new",
        templateUrl: 'templates/settings/newAlert.html'
    }).
    state('content.settings.conditions.notifications', {
        url: "/notifications/all",
        templateUrl: 'templates/settings/notificationsAll.html'
    }).
    state('content.settings.conditions.newNotification', {
        url: "/notifications/new",
        templateUrl: 'templates/settings/notificationsNew.html'
    }).
    state('content.settings.conditions.actions', {
        url: "/actions/all",
        templateUrl: 'templates/settings/actionsAll.html'
    }).
    state('content.settings.conditions.newActions', {
        url: "/actions/new",
        templateUrl: 'templates/settings/actionsNew.html'
    }).
    state('content.settings.accessControl', {
        url: "/accessControl",
        templateUrl: 'templates/settings/accessControl.html'
    }).
    state('content.settings.accessControl.conditions/alerts/allInspector', {
        url: "/subInspector",
        templateUrl: 'templates/settings/subInspector.html'
    }).
    state('content.settings.accessControl.circleInspector', {
        url: "/circleInspector",
        templateUrl: 'templates/settings/circleInspector.html'
    }).
    state('content.settings.accessControl.assiSubInspector', {
        url: "/assiSubInspector",
        templateUrl: 'templates/settings/assiSubInspector.html'
    });

    $urlRouterProvider.otherwise("/map/live")
});

suratCam.controller('fullPage', function ($scope, $rootScope) {

    $scope.test = function (active) {
        console.log("from callBack " + active)
    }
});


