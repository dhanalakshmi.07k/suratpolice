/**
 * Created by MohammedSaleem on 20/11/15.
 */

(function () {

    var appSlider= angular.module('appSlider',[]);


    appSlider.directive("dateSlider", function () {

        var link = function (scope, element, attribute) {

            //..... default settings.....
            var width = scope.width;
            var slider = $(element).find(".slider");
            var container = slider.find(".sliderData");

            var label,labelWidth,totalLabels,sliderWidth,containerWidth,maxSlideVal;


            var sliderDropDown= scope.dropDown;

            if (sliderDropDown){
                if(sliderDropDown=='true'){
                    $(element).find(".dateSliderMain").addClass("dependent");
                    var dateDropDown='<div class="dropDown midBlock small year">'+
                        '<div class="head" data-init="0">'+
                        '<div class="headTitle">2015</div>'+
                        '<img src="directives/images/downArrowLightGrey.png" alt="down" class="sign">'+
                        '</div>'+
                        '<ul class="list">'+
                        '<li>2013</li>'+
                        '<li>2014</li>'+
                        '<li>2015</li>'+
                        '</ul>'+
                        '</div>'+

                        '<div class="dropDown midBlock small month">'+
                        '<div class="head" data-init="0">'+
                        '<div class="headTitle">Nov</div>'+
                        '<img src="directives/images/downArrowLightGrey.png" alt="down" class="sign">'+
                        '</div>'+
                        '<ul class="list">'+
                        '<li>Jan</li>'+
                        '<li>Feb</li>'+
                        '<li>Mar</li>'+
                        '<li>Apr</li>'+
                        '<li>May</li>'+
                        '<li>Jun</li>'+
                        '<li>Jul</li>'+
                        '<li>Aug</li>'+
                        '<li>Sep</li>'+
                        '<li>Oct</li>'+
                        '<li>Nov</li>'+
                        '<li>Dec</li>'+
                        '</ul>'+
                        '</div>';

                    $(dateDropDown).prependTo($(element).find(".dateSliderMain"));
                }
            }


            var constructDay_DropDown= function () {
                var addDays= function () {
                    var year=$(element).find(".dropDown.year .headTitle").text().trim();
                    var month=$(element).find(".dropDown.month .headTitle").text().trim();
                    var numberOfDays=moment(year+"-"+month, "YYYY-MMM").daysInMonth();

                    console.log(numberOfDays);
                    for(i=1;i<=numberOfDays;i++){
                        if(i<10){
                            i="0"+i;
                        }
                        $('<div class="label">'+i+" "+month+'</div>').appendTo(container);
                    }
                    $('<span class="clear"></span>').appendTo(container);
                };
                addDays();

                $(element).find(".dropDown .list li").click(function () {
                    setTimeout(function () {
                        container.html("");
                        typeBaseFunctionality();
                        sliderInitializer();
                    },50);
                });
            };




            var constructHour= function(){
                for(i=1;i<=24;i++){
                    $('<div class="label">'+i+'</div>').appendTo(container);
                }
                $('<span class="clear"></span>').appendTo(container);
            };



            var sliderInitializer= function () {
                label = container.find(".label");
                labelWidth = label.first().width();
                totalLabels = container.find(".label").length;
                sliderWidth = slider.width();

                containerWidth = labelWidth * totalLabels;
                maxSlideVal = containerWidth - sliderWidth;

                container.css({
                    width: containerWidth
                });
                var minSlideVal;
                container.draggable({
                    axis: "x",
                    stop: function (event, ui) {
                        container = $(this);
                        minSlideVal = container.position().left;
                        if (minSlideVal > 0) {
                            container.animate({
                                left: 0
                            }, 400)
                        }
                        else if (minSlideVal < -maxSlideVal) {
                            container.animate({
                                left: -maxSlideVal
                            }, 400)
                        }
                    }
                });
            };




            //........ add Slider navigation ex: navigation= 'true or false' .......

            var navigation=scope.navigation;
            if(scope.navigation){
                if(navigation=='true'){
                    var navEle='<div class="arrow leftArrow">'+
                        '<img src="images/leftArrowGrey.png" alt="">'+
                        '</div>'+
                        '<div class="arrow rightArrow">'+
                        '<img src="images/rightArrowGrey.png" alt="">'+
                        '</div>';


                    $(element).addClass("navigate")
                        .find(".dateSlider").append(navEle);
                }

            }

            //...... settings based on slider type 'range or select or hour'......


            var typeBaseFunctionality= function () {
                var sliderType=(scope.sliderType).trim();
                if(sliderType){
                    if(sliderType=='range'){
                        constructDay_DropDown();
                        sliderInitializer();



                        $('<div class="selector"></div>').prependTo(container);

                        var firstLabel;
                        var lastLabel;
                        var selector = container.find(".selector");

                        selector.css({
                            width: labelWidth * width,
                            left: labelWidth
                        });


                        var showSelection = function () {
                            var selectorLeftVal = Math.round(selector.position().left);
                            var selectorWidth = selector.outerWidth();

                            firstLabel = selectorLeftVal / labelWidth;
                            lastLabel = (selectorLeftVal + selectorWidth) / labelWidth;

                            label.removeClass("selected");

                            for (var i = firstLabel; i < lastLabel; i++) {
                                label.eq(i).addClass("selected");
                            }
                            scope.rangeStart = firstLabel;
                            scope.rangeEnd = lastLabel;
                        };

                        var resizing = function () {
                            selector.resizable({
                                handles: "e, w",
                                distance: 5,
                                grid: [labelWidth, 0],
                                containment: "parent",
                                resize: function (event, ui) {
                                    showSelection();
                                    scope.$apply();
                                    if (scope.callbackOnSelection()) {
                                        scope.callbackOnSelection()(scope.rangeStart, scope.rangeEnd)
                                    }
                                }
                            });
                        };
                        showSelection();
                        resizing();
                    }



                    else if(sliderType=="hour"){
                        $(element).find(".dateSlider").addClass("timer");
                        var indicator='<div class="hourHolder roboRegular">'+
                            '<div class="timeBox smallest">'+
                            '<div class="midBlock hour">09:15</div>'+
                            '<div class="midBlock now">Now</div>'+
                            '</div>'+
                            '<div class="holder"></div>'+
                            '</div>';


                        constructHour();
                        sliderInitializer();
                        $(indicator).appendTo(container);
                        $('<span class="clear"></span>').appendTo(container);
                    }



                    else if(sliderType=="rangeHour"){
                        constructHour();
                        sliderInitializer();


                        $('<div class="selector"></div>').prependTo(container);

                        var firstLabel;
                        var lastLabel;
                        var selector = container.find(".selector");

                        selector.css({
                            width: labelWidth * width,
                            left: labelWidth
                        });


                        var showSelection = function () {
                            var selectorLeftVal = Math.round(selector.position().left);
                            var selectorWidth = selector.outerWidth();

                            firstLabel = selectorLeftVal / labelWidth;
                            lastLabel = (selectorLeftVal + selectorWidth) / labelWidth;

                            label.removeClass("selected");

                            for (var i = firstLabel; i < lastLabel; i++) {
                                label.eq(i).addClass("selected");
                            }
                            scope.rangeStart = firstLabel;
                            scope.rangeEnd = lastLabel;
                        };

                        var resizing = function () {
                            selector.resizable({
                                handles: "e, w",
                                distance: 5,
                                grid: [labelWidth, 0],
                                containment: "parent",
                                resize: function (event, ui) {
                                    showSelection();
                                    scope.$apply();
                                    if (scope.callbackOnSelection()) {
                                        scope.callbackOnSelection()(scope.rangeStart, scope.rangeEnd)
                                    }
                                }
                            });
                        };
                        showSelection();
                        resizing();

                    }

                    else if(sliderType=="select"){
                        constructDay_DropDown();
                        sliderInitializer();

                        var activeIndex=0;
                        container.find(".label").eq(activeIndex).addClass("active");

                        container.find(".label").click(function () {
                            container.find(".label").removeClass("active");
                            $(this).addClass("active");

                            activeIndex=$(this).index();
                            scope.activeDate= activeIndex;

                            if (scope.callbackOnSelection()) {
                                scope.callbackOnSelection()(scope.activeDate)
                            }
                        });
                    }
                }
                else{
                    console.log("slider type is not defined");
                }
            };

            typeBaseFunctionality();

        };
        return {
            templateUrl: "directives/templates/timeSliderTemplate.html",
            scope: {
                width: '@',
                sliderType:'@',
                rangeStart: '=',
                rangeEnd: '=',
                callbackOnSelection: '&',
                activeDate:'=',
                navigation:'@',
                dropDown:'@',
                arr:"="
            },
            link: link
        }
    })

})();

