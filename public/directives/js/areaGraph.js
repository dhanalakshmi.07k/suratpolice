/**
 * Created by MohammedSaleem on 21/11/15.
 */

(function () {

    var areaGraph= angular.module('areaGraph',[]);

    var link= function (scope, element, attribute) {
        $(element).find(".areaSpline").highcharts({
            chart: {
                type: 'areaspline'
            },
            legend: {
                enabled: false
            },
            title: {
                text: ''
            },

            xAxis: {
                //categories: ['26 Oct','27 Oct','28 Oct','29 Oct','30 Oct'],
                categories: scope.x,
                title: {
                    text: 'Days',
                    align: 'high'
                }
            },
            yAxis: {
                title: {
                    text: 'Occupancy',
                    align: 'low'
                },
                //floor: 0,
                //ceiling: 100,
                gridLineDashStyle: 'Dot',
                gridLineWidth: 1,
                gridLineColor: '#719baf',
                labels: {
                    formatter: function () {
                        return this.value + '';
                    }
                }
            },
            tooltip: {
                shared: true,
                valueSuffix: ' units'
            },
            credits: {
                enabled: false
            },
            plotOptions: {
                areaspline: {
                    fillOpacity: 0.7
                },
                series: {
                    lineColor: '#525b62',
                    lineWidth: 1,
                    marker: {
                        fillColor: '#FFFFFF',
                        lineWidth: 1,
                        lineColor: '#525b62'
                    }
                }

            },
            series: [{
                data: scope.y,
                color: '#f6d5d7',
                states: {
                    hover: {
                        enabled: false
                    }
                }
            }]
        });


      var time;
      var estimate;
          var chart=$(element).find(".areaSpline").highcharts();
          var chartSeries = chart.series[0];

          socket.on('auto', function (dataMap) {
            var autoData = dataMap.data.autorickshawCount[0];
             var Time =new Date(dataMap.data["frameNumber"]);
             /* var options = {
                hour: "2-digit", minute: "2-digit"
              };*/
             /*time = Time.toLocaleDateString("en-US",options);*/
            time = Time.getHours()+":"+Time.getMinutes();
            console.log(time);
             estimate =parseInt(autoData.estimate);
            var seriesMove = chartSeries.data.length > 8;
            chartSeries.addPoint([time ,estimate], true, seriesMove);
            console.log(time)
          });



    };

    areaGraph.directive('areaSpline', function () {
        return {
            templateUrl: 'directives/templates/areaGraph.html',
            link: link,
            scope:{
                x:"=",
                y:"="
            }
        }
    });


})();
