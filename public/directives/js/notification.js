/**
 * Created by MohammedSaleem on 20/11/15.
 */

(function () {

    var appNotification= angular.module('appNotification',[]);/*'suratCamControllers.settingsController'*/

    appNotification.directive('notification', function () {
      var link = function(scope,element,atrrs){
        /*console.log(scope.messagesDataModel)*/
         scope.dateNow = function(){return new Date()}

        $("#appWrapper").on("click",".notificationList li",function () {
          var init= $(this).data("init");
          if(init=="0"){
            $(this).find(".notifDetails").slideDown(300);
            $(this).data({
              "init":"1"
            });
          }
          else{
            $(this).find(".notifDetails").slideUp(300);
            $(this).data({
              "init":"0"
            });
          }
        })
      }
        return {
            replace: true,
            templateUrl: 'directives/templates/notifications.html',
            scope: {
              messagesDataModel: "=",
              clearMe:"=",
              filterType:"=",
              focusOn:"="
            },
          link: link
        }
    });
    /*appNotification.filter("messageFilter", function(){
      return function(input) {
        var filteredArray =[];
        for(var i=0;i<input.length;i++){
          if(input[i].type==='Loitering'){
            filteredArray.push(input[i]);
          }
        }
        return filteredArray;
      }
    })*/

})();

