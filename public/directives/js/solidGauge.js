/**
 * Created by MohammedSaleem on 21/11/15.
 */

(function () {

    var circleGauge= angular.module('circleGauge',[]);

    var link= function (scope, element, attribute) {
        var dataValue=scope.value;
        var maxVal,color,textColor,endingAngle;

        if (dataValue<0){
            maxVal=-100;
            color='#00a27a';
            textColor='gaugeDec';
            endingAngle=-360;
        }
        else{
            maxVal=100;
            color='#e21731';
            textColor='gaugeInc';
            endingAngle=360;
        }

        var gaugeOptions = {

            chart: {
                type: 'solidgauge'
            },

            title: null,

            pane: {
                center: ['50%', '53%'],
                size: '100%',
                startAngle: 0,
                endAngle: endingAngle,
                background: {
                    backgroundColor: '#e8ecf2',
                    innerRadius: '88%',
                    outerRadius: '100%',
                    borderWidth: 0,
                    shape: 'arc'
                }
            },

            tooltip: {
                enabled: false
            },

            // the value axis
            yAxis: {
                min: 0,
                max: maxVal,
                showFirstLabel:false,
                showLastLabel:false,
                lineWidth: 0,
                minorTickInterval: null,
                tickPixelInterval: 400,
                tickWidth: 0
            },

            plotOptions: {
                solidgauge: {
                    dataLabels: {
                        y: -20,
                        borderWidth: 0,
                        useHTML: true
                    }
                }
            },
            credits: {
                enabled: false
            }
        };





        $(element).find(".solidGauge").highcharts(Highcharts.merge(gaugeOptions, {
            series: [{
                name: 'Speed',
                data: [{
                    radius: 100,
                    innerRadius: 88,
                    color: color,
                    y: scope.value
                }],
                dataLabels: {
                    format: '<div class="roboLight '+textColor+'"><span class="midTitle midBlock">{y}%</span></div>'
                }
            }]

        }));


    };

    circleGauge.directive('solidGauge', function () {
        return {
            templateUrl: 'directives/templates/solidGauge.html',
            link: link,
            scope:{
                value:"="
            }
        }
    });


})();
