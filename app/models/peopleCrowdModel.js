/**
 * Created by zendynamix on 01-12-2015.
 */

var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

var peopleCrowdSchema = new mongoose.Schema(
  {
    "crowdCount": [
      {
        regionName:String,
        "estimate":String,
        "level":String,
       /* "latitude":Number,
        "longitude": Number*/
      }
    ],
    "cameraID": String,
    "cameraLatitude": String,
    "cameraLongitude":String,
    "cameraName": String,
    "frameNumber":String,
    "mobId":String
  },{collection: "peopleCrowdSchema"});


/*+++++++++++++++++++++++++++++++++++++++++  Defining Model  Starts++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
mongoose.model('peopleCrowdModel',peopleCrowdSchema);
