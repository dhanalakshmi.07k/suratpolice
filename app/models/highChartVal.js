/**
 * Created by Suhas on 11/24/2015.
 */
var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

var highChartValueSchema = new mongoose.Schema(
  {
    /*notifyId:Number,*/
    valId: Number,
    val:Array
  },{collection: "highChartValueSchema"});
/*+++++++++++++++++++++++++++++++++++++++++  Defining Model  Starts++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
mongoose.model('highChartVal',highChartValueSchema);
