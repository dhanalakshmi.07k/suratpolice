/**
 * Created by Suhas on 11/27/2015.
 */
var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

var wantedSchema = new mongoose.Schema(
  {
    "Person Of Interest":{
      "Camera Name": String,
      personId:Number,
      "Photo":String,
      "Person ID":String,
      "Camera ID": String,
      "Latitude":String,
      "Longitude": String,
      "Time Stamp": Date,
      personId:Number
    }
  },{collection: "wantedSchema"});


/*+++++++++++++++++++++++++++++++++++++++++  Defining Model  Starts++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
mongoose.model('wantedPerson',wantedSchema);
