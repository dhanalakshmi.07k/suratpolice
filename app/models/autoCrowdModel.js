/**
 * Created by Suhas on 11/28/2015.
 */
var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

var autoCrowdSchema = new mongoose.Schema(
  {
    "autorickshawCount":[
      {
        regionName:String,
        "estimate":String,
        "level":String,
       /* "latitude":Number,
        "longitude": Number*/
      }
    ],
    "cameraID": String,
    "cameraLatitude":Number,
    "cameraLongitude": Number,
    cameraName: String,
    autoId:Number,
    "frameNumber": String,
    "Time Stamp":Date

  },{collection: "autoCrowdSchema"});


/*+++++++++++++++++++++++++++++++++++++++++  Defining Model  Starts++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
mongoose.model('autoCrowdModel',autoCrowdSchema);
