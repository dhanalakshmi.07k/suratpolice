/**
 * Created by Suhas on 11/26/2015.
 */
var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

var loiteringSchema = new mongoose.Schema(
  {
    "Loiterer":
    {
      "camera Name": String,
      personId:Number,
      "Photo":String,
      Latitude:String,
      Longitude: String,
      "Camera ID": String,
      "Time Stamp":String
    }
  },{collection: "loiteringSchema"});


/*+++++++++++++++++++++++++++++++++++++++++  Defining Model  Starts++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
mongoose.model('loiteringPerson',loiteringSchema);
