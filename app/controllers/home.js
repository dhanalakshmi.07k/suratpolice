var express = require('express'),
  router = express.Router(),
  mongoose = require('mongoose'),
   jwt    = require('jsonwebtoken'),// used to create, sign, and verify tokens
   expressJwt = require('express-jwt'), //https://npmjs.org/package/express-jwt
   secret = 'this is the secrete password';
  Article = mongoose.model('Article'),
  LoiteringPerson = mongoose.model('loiteringPerson'),
  WantedPerson = mongoose.model('wantedPerson'),
  autoCrowdModel = mongoose.model('autoCrowdModel'),
  highChartVal = mongoose.model('highChartVal'),
  AutoCrowdModel = mongoose.model('autoCrowdModel');
  peopleCrowdSchema = mongoose.model('peopleCrowdModel');
module.exports = function (app) {
  app.use('/', router);

};


router.use('/api', expressJwt({secret: secret}));
router.use(function(err, req, res, next){
  if (err.constructor.name === 'UnauthorizedError') {
    res.status(401).send('Unauthorized Token Is not present');
  }
});

var SocketIo = require('../../config/express');
router.get('/', function (req, res, next) {
  Article.find(function (err, articles) {
    if (err) return next(err);
    res.render('index', {
      title: 'Generator-Express MVC',
      articles: articles
    });
  });
});
router.get('/loiteringNotifySimulator', function (req, res, next) {
  var io = req.io;
  var person_Id = [0,1,2,3];
  var person_Id_obj = person_Id[Math.floor(Math.random()*person_Id.length)]
  LoiteringPerson.findOne({"Loiterer.personId":person_Id_obj},function(err,result){
    var loiteringData = result;

    updateLoiteringData(loiteringData);
    res.send(result);
  })


});
router.get('/wantedNotifySimulator', function (req, res, next) {
  var io = req.io;
  var person_Id = [0,1,2,3,4];
  var person_Id_obj = person_Id[Math.floor(Math.random()*person_Id.length)]
  WantedPerson.findOne({"Person Of Interest.personId":person_Id_obj},function(err,result){
    var wantedData = result;
    updateWantedPersonData(wantedData);
    res.send('');
  })


});
router.get('/autoNotifySimulator', function (req, res, next) {
  var io = req.io;
  var auto_Id = [0,1,2];
  var auto_Id_obj = auto_Id[Math.floor(Math.random()*auto_Id.length)]
  AutoCrowdModel.findOne({"autoId":auto_Id_obj},function(err,result){
    var autoCrowdData=result;
    updateAutoCrowdData(autoCrowdData)
    res.send(result);
  })


});
router.get('/peopleNotifySimulator', function (req, res, next) {
  var io = req.io;
  var mobId = [0,1,2];
  var mob_Id_obj = mobId[Math.floor(Math.random()*mobId.length)]
  peopleCrowdSchema.findOne({"mobId":mob_Id_obj},function(err,result){
    var peopleCrowdData=result;
    updatePeopleCrowdData(peopleCrowdData)
    res.send("mobCrowd");
    console.log(peopleCrowdData);
  })

});

function updateLoiteringData(loiteringData){
  SocketIo.emit('loitering',{
    data:loiteringData
  })
}
function updateWantedPersonData(wantedData){
  SocketIo.emit('wanted',{
    data:wantedData
  })
}
function updateAutoCrowdData(autoCrowdData){
  SocketIo.emit('auto',{
    data:autoCrowdData
  })
}
function updatePeopleCrowdData(peopleCrowdData){
  SocketIo.emit('mobCrowd',{
    data:peopleCrowdData
  })
}
router.get('/highChartVal/start/:start/end/:end', function (req, res, next) {
  highChartVal.find({valId:{$gte: req.params.start, $lt: req.params.end}},function(err,highChartVal){
    res.send(highChartVal);
  })
});
