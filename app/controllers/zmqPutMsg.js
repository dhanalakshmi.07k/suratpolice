/**
 * Created by rranjan on 11/29/15.
 */

var express = require('express');
var mongoose = require('mongoose');
var zmq = require('zmq');
var config = require('../../config/config');

var HashTable = require('hashtable');
var portHashtable = new HashTable();

//var SocketIo = require('../../config/express');

var router = express.Router();
var zmqPubSocket = zmq.socket('pub');

/*
 portPOI:'4002',
 portLoitering:'4003',
 portAuto:'4004',
 portCrowd:4005
 */



module.exports = function (app) {
  app.use('/', router);
};


router.post('/putMsgIn0MQ', function (req, res, next) {
  pushData("portAuto",JSON.stringify(req.body))
  res.end()

});

router.post('/putMsgIn0MQloitering', function (req, res, next) {
  pushData("portLoitering",JSON.stringify(req.body))
  res.end()

});

router.post('/putMsgIn0MQwanted', function (req, res, next) {
  pushData("portPOI",JSON.stringify(req.body))
  res.end()

});

router.post('/putMsgIn0MQauto', function (req, res, next) {
  pushData("portAuto",JSON.stringify(req.body))
  res.end()

});

router.post('/putMsgIn0MQCrowd', function (req, res, next) {
  pushData("portCrowd",JSON.stringify(req.body))
  res.end()

});
var pushData = function(portName ,dataToPush){
  console.log(dataToPush)
  var zmqSocket = getPort(portName)
  zmqSocket.send(dataToPush);

}

function getPort(portName){

  var port = portHashtable.get(portName);
  console.log("port is %s",port);

  if(!port){
    console.log("port is %s", port);
    console.log("creating a new port");
    var zmqSocket = zmq.socket('push');
    var zmqPortPart = 'tcp://' + config.zmq.sendHost + ':' //+config.zmq.port
    zmqSocket.connect(zmqPortPart + config.zmq[portName])
    port = zmqSocket;
    portHashtable.put(portName,port);
  }
  console.log("returning port :");
  console.log(port);
  return port;

}
