/**
 * Created by Suhas on 4/18/2016.
 */
var ProtoBuf =require("protobufjs");
var builder = ProtoBuf.loadProtoFile(__dirname+"/protoFiles/Events.proto");/*
 var commonBuilder = ProtoBuf.loadProtoFile(__dirname+"/protoFiles/Common.proto");*/
var Event =builder.build("com.nec.eva.msap.proto.events.Event");
var FaceEvent =builder.build("com.nec.eva.msap.proto.events.FaceEvent");
var ObjectType =builder.build("com.nec.eva.msap.proto.common.ObjectType");
var ClientEvent = builder.build("com.nec.eva.msap.proto.events.CrowdEvent");
var CrowdStat =builder.build("com.nec.eva.msap.proto.events.CrowdStat");
var Rect =builder.build("com.nec.eva.msap.proto.common.Rect");
var fs = require('fs');
var SocketIo = require('../../config/express');
var pulseAlertData = function(data){
  if(data){
    var eventObj = Event.decode(data);
    if(eventObj.face_event){
      pushFaceEventData(eventObj.face_event);
    }else{
      pushCrowdEventData(eventObj.crowd_event);
    }
  }

}
function pushCrowdEventData(data){
  console.log(data)
console.log("Crowd event")
}
function pushFaceEventData(data){
  var personOfInterest = {
    "Person Of Interest":{
      "Camera Name": "1",
      personId:1,
      "Photo":"xyz",
      "Person ID":"event",
      "Camera ID": "123",
      "Latitude":"12123",
      "Longitude": "asd",
      "Time Stamp": new Date(data.timestamp.low)
    }
  }
  /*SocketIo.emit('wanted',{
    data:personOfInterest
  })*/
  /*console.log("Event")*/
  /*console.log(data)*/
  /*console.log(new Date(data.timestamp.low))*/

}
module.exports={
  buildObject:pulseAlertData
}
