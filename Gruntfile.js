'use strict';

var request = require('request');

module.exports = function (grunt) {
  // show elapsed time at the end
  require('time-grunt')(grunt);
  // load all grunt tasks
  require('load-grunt-tasks')(grunt);

  var reloadPort = 35729, files;

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    develop: {
      server: {
        file: 'app.js'
      }
    },
    watch: {
      options: {
        nospawn: true,
        livereload: reloadPort
      },
      js: {
        files: [
          'app.js',
          'app/**/*.js',
          'config/*.js'
        ],
        tasks: ['develop', 'delayed-livereload']
      },
      css: {
        files: [
          'public/css/*.css'
        ],
        options: {
          livereload: reloadPort
        }
      },
      views: {
        files: [
          'app/views/*.jade',
          'app/views/**/*.jade'
        ],
        options: { livereload: reloadPort }
      }
    },
    uglify: {
             my_target: {
                     options: {mangle: false},
                            files: {
                            '../suratpoliceDistribution/public/minifiedjs/minjs-min.js':['public/js/lib/angular.min.js',
                                                                                                   'public/js/lib/angular-ui-router.js',
                                                                                                   'public/js/app.js',
                                                                                                   'public/js/controllers.js',
                                                                                                   'public/js/controllers/notificationCtrl.js',
                                                                                                   'public/js/controllers/analyticsCtrl.js',
                                                                                                   'public/js/controllers/geoFenceCtrl.js',
                                                                                                   'public/js/controllers/liveCtrl.js',
                                                                                                   'public/js/controllers/activeNavCtrl.js',
                                                                                                   'public/js/controllers/trafficCtrl.js',
                                                                                                   'public/js/controllers/settingsCtrl.js',
                                                                                                   'public/js/controllers/loginCtrl.js',
                                                                                                   'public/directives/js/dateSlider.js',
                                                                                                   'public/directives/js/notification.js',
                                                                                                   'public/directives/js/sideNavigation.js',
                                                                                                   'public/directives/js/areaMap.js',
                                                                                                   'public/directives/js/areaGraph.js',
                                                                                                   'public/directives/js/solidGauge.js',
                                                                                                   'public/directives/js/highChart.js',
                                                                                                   'public/js/service/analyticsService.js',
                                                                                                   'public/js/service/settingService.js',
                                                                                                   'public/js/service/liveDataService.js',
                                                                                                   'public/js/service/AuthenticationService.js'],

                            '../suratpoliceDistribution/public/minifiedjs/jQuery-min.js':['public/js/lib/jquery-2.1.4.min.js',
                                                                                       'public/js/lib/jquery-ui.min.js',
                                                                                       'public/js/lib/highcharts.js',
                                                                                       'public/js/lib/highcharts-more.js',
                                                                                       'public/js/lib/highcharts-solid-gauge.js',
                                                                                       'public/js/lib/moment.js',
                                                                                       'public/js/script.js'],

                                                           }
                          }
            },
    cssmin: {
              target: {
                        files: {
                          '../suratpoliceDistribution/public/minifiedcss/minCss-min.css': ['public/css/uiStyle.css',
                                                                                           'public/css/prestyles.css',
                                                                                           'public/css/lib/jquery-ui.min.css',
                                                                                           'public/directives/css/dateRangeSlider.css',
                                                                                           'public/css/lib/animate.css',
                                                                                           'public/css/fonts.css']
                        }
                      }
            },
    processhtml: {
                 build: {
                        files: {
                           '../suratpoliceDistribution/public/index.html': ['public/index.html']
                         }
                       }
                 },
    copy: {
             main: {
                   files: [
                           {expand: true, cwd: 'app/', src: ['**'], dest: '../suratpoliceDistribution/app/'},
                           {expand: true, cwd: 'config/', src: ['**'], dest: '../suratpoliceDistribution/config/'},
        /* {expand: true, cwd: 'public/app', src: ['**'], dest: '../suratpoliceDistribution/public/app'},*/
                           {expand: true, cwd: 'public/', src: ['**','!**/css/**','!**/directives/css/**','!**/directives/js/**','!**/js/controllers/**','!**/js/controllers.js','!**/js/lib/angular.min.js','!**/js/lib/angular-ui-router.js',
                                                                 '!**/js/lib/highcharts-solid-gauge.js','!**/js/lib/jquery-2.1.4.min.js','!**/js/lib/jquery-ui.min.js','!**/js/lib/highcharts.js','!**/js/lib/highcharts-more.js','!**/js/lib/moment.js',
                                                                 '!**/js/app.js','!**/js/script.js','!**/js/service/**','!index.html'], dest: '../suratpoliceDistribution/public/'},
                           { src: "package.json", dest: '../suratpoliceDistribution/'},
                           { src:"app.js", dest: '../suratpoliceDistribution/'},
                           { src:"pm2.json", dest: '../suratpoliceDistribution/'}
                          ]
                       }
          }
  });

  grunt.config.requires('watch.js.files');
  files = grunt.config('watch.js.files');
  files = grunt.file.expand(files);
  // load required npm plugins
      grunt.loadNpmTasks('grunt-contrib-uglify');
      grunt.loadNpmTasks('grunt-processhtml');
      grunt.loadNpmTasks('grunt-contrib-copy');
      grunt.loadNpmTasks('grunt-contrib-cssmin');

  grunt.registerTask('delayed-livereload', 'Live reload after the node server has restarted.', function () {
    var done = this.async();
    setTimeout(function () {
      request.get('http://localhost:' + reloadPort + '/changed?files=' + files.join(','),  function(err, res) {
          var reloaded = !err && res.statusCode === 200;
          if (reloaded)
            grunt.log.ok('Delayed live reload successful.');
          else
            grunt.log.error('Unable to make a delayed live reload.');
          done(reloaded);
        });
    }, 500);
  });

  grunt.registerTask('default', [
    'develop',
    'watch'
  ]);
  grunt.registerTask('production',['cssmin','uglify','processhtml','copy']);
};
