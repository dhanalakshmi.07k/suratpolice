

var express = require('express'),
  config = require('./config/config'),
  glob = require('glob'),
  mongoose = require('mongoose');

mongoose.connect(config.db);
var db = mongoose.connection;
db.on('error', function () {
  throw new Error('unable to connect to database at ' + config.db);
});

var models = glob.sync(config.root + '/app/models/*.js');
models.forEach(function (model) {
  require(model);
});
var app = express();

var server = require('http').Server(app);
var io = require('socket.io')(server);

server.listen(config.port, function () {
  console.log('Express server listening on port ' + config.port);
});

var socket

io.on('connection', function (newSocket) {
  console.log("welcome .......")
  socket=newSocket

  socket.emit('news', { hello: 'world' });
  socket.on('ClientMsg', function (data) {
    console.log(data);
  });
});
require('./config/express')(app,config,io);
//require('./config/zmq')(config,io);
require('./config/zmqPushPull').zmqPushPull(config,io);

/*
 var zmq = require('zmq');

 console.log("===========================");
 //console.log(io);
 console.log("===========================");
 var zmqsock = zmq.socket('sub');
 zmqsock.connect('tcp://localhost:4001');
 zmqsock.subscribe('msg');

 console.log("sub to msg");

 zmqsock.on("message",function(topic,message ){
 console.log("new message")
 console.log(message.toString());
 //io.emit('msg',JSON.parse(message.toString()))
 })

 */
/*require('./app/controllers/home.js')(io);*/
